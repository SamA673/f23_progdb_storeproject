# SuperStore DB Manager
SSDB is an efficient, easy and convenient way to interact with your Oracle Store database!

Designed and created by David Parker (2139469) & Sam A (2043024)
## Installation

1. Download the project from the following link! 
```link
'https://gitlab.com/SamA673/f23_progdb_storeproject'
```
2. Open your SQL interaction application (IE: SQL Developer)
and run the following scripts to install all the tables, types, triggers and the Package, which contains procedures and functions that are vital to the application. 
```bash
'Installer.sql'
'Super_Store.sql'
```

3. Make sure all of your project .java files are compiled. You may do so using your IDE (IE: Visual Studio Code) or a terminal with javac. The files SHOULD compile into the target/ folder.


## Usage

1. Start the app using your preferred terminal window! (IDE, GitBash, etc)

2. When prompted, log into the server using your Oracle Username and password.

3. You will be presented with a main menu with many different features. 
```text
------------ DATABASE CLIENT ------------
Hi Admin <YourUserName>
What would you like to do?
1: Display
2: Add
3: Update
4: Delete
5: Moderate Reviews
0: Quit
```

Press the number corresponding to the action you want to use. 


### Displaying
Displaying tables in SSDB is fairly straight forward.
You are initially greeted by the following screen. 

```text
--------------------------- DATABASE CLIENT 1.0 ---------------------------
What do you wanna display?
1: Display products (All or by category)
2: Display total quantity of a product across warehouses
3: Display orders (All or filtered by CustomerId, Date, Store)
4: Display audit log
5: Display average review of a product
0: I'm done displaying!

```
#### Products (All or by category)

You will be asked to provide a category. You may leave it blank if you don't want any category filtering. 
#### Display total quantity 
This will display the total quantity of a product available across the database. 

#### Display orders 

This will display all the orders. You can, again, insert filters when prompted to narrow your search down. (Or decline by entering 'n' to the prompt). 

#### Display Audit Log
This will display all the changes made to the database, from inserts to updates to deletes!


#### Display Average review of a product
This will display the average review of a given product, according to all the reviews available in the database.


### Adding
Adding is just as easy. Here is the menu:

```text
--------------------------- DATABASE CLIENT 1.0 ---------------------------
What would you like to add?
1. Product
2. Customer
3. Store
4. Warehouse
5. Orders
6. Review
7. Inventory
8. ProductStore
9. OrdersProduct
0. I'm done adding!
```
Just press the number associated to the table you want to add to, and answer the prompts. If there is a problem, your custom error message will guide you.


### Updating
Here is the update menu!
```text
--------------------------- DATABASE CLIENT 1.0 ---------------------------
What would you like to update?
1. Warehouse Product Inventory Quantity
2. Reviews with 1 flag and under
3. Product Info (Category or Name)
0: I'm done updating!
```

#### Warehouse Product Inventory Quantity
This is to update stock information at a certain warehouse. Say for example warehouse 3 gets a shipment of 300 bananas. The inventory table must reflect that. This is the perfect case for the Warehouse Product Inventory Quantity updater!

#### Review Message with 1 fla and under
This is to change the message of a review

#### Product Info
This is to change the category or the name of a product. 

### Deleting 
```text
--------------------------- DATABASE CLIENT 1.0 ---------------------------
What would you like to delete?
1. Order
2. Reviews with 1 flag and under
3. Warehouse
0. I'm done deleting!
```

Follow the prompts to delete the given row! 

### Moderate Flagged reviews
Here, you can moderate your reviews! 

--------------------------- DATABASE CLIENT 1.0 ---------------------------
What would you like to do?
1. Check flagged reviews
2. Check problematic customers
3. Update overly flagged reviews
4. Delete overly flagged reviews
0. I'm done moderating!

## Database Design
It's important to understand how the database itself works. Here's our design ERD! 


![alt text](https://cdn.discordapp.com/attachments/1014723235037851768/1177107292546662471/SQLFinalERD.png?ex=65714d98&is=655ed898&hm=e220f917311537889b3b9c6a9657052d4800ceea70c86c4e6b241e18b59ce639&)

### Warehouse

Warehouses are locations with a name, address and Id.

### Inventory
Each warehouse has quantities of products. The quantity can be found in the Inventory table, which links the Warehouse table to the Product table. 

### Product
Products are the main piece of the whole database. A table row contains a unique Id, a name, a category, a price and an average review, which is updated anytime a new review is created for that product. 

### Review
Reviews are what allows our customers to make informed decisions. The review table holds information about who the reviewer is (CustomerId), what product they are reviewing, the score they give (if any) and a description.

### Customer
The customer table contains all information about a customer. It's linked to reviews and to orders so that Customers can place an order or create a review on a product they have bought. The fields are First Name, LastName, Email and Address. 

### Orders
The orders table contains information about the customer, the store and the date of the order. An order can be comprised of 1 or many Order_Products (IE: John creates an order(ID: 123 at store 999 on 2020-10-10 and then buys 5 Shoes, 2 Pumpkins and 1 hat. The 3 products he bought at Store 999 will each be a Order_Product row.

### Orders_Product
The Orders_Product table, as explained in orders, contains information on the quantity of an item someone bought and the order id it's linked to. An order can contain many Orders_Product, but each Orders_Product can only be for 1 order. 

### Product_Store
Not every store sells shoes! Product_Store keeps track of what products each store is selling to customers. 

### Audit Table
Finally, the audit table keeps track of the time, type of change, user, old value, new value and the table that is affected during a change. 


## Deleting 

1. Open up your SQL Developer and run the following script:
```bash
'Uninstaller.sql'
```

2. Delete the project file! (f23_progdb_storeproject)! 

