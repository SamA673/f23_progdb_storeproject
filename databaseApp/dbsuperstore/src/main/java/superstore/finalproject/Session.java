package superstore.finalproject;
import java.sql.*;

/**
 * The Session class manages connections to an SQL database.
 * It provides methods to interact with the database. 
 *
 * @author Sam A
 * @version 1.0
 * @Date November 15, 2023
 */

public class Session{
    private String url; 
    private User currentUser; 
    private Connection conn; 
    public static final String password = "123"; 
    // private boolean isLive; 

    /**
     * Constructs a session with a given url. 
     *
     * @param url URL of the remote SQL database. 
     */
    public Session(String url) {
        this.url = url; 
        this.currentUser = null;
        this.conn =  null;
        // isLive = false; 
    }

    /**  
     * Gets the URL of the current SQL database. 
     * @return The URL of the current SQL database. 
     */
    public String getUrl() {
        return url;
    }

    /**  
     *  Gets the current user. 
     *  @return      The current SQL database user.
     **/
    public User getCurrentUser() {
        return currentUser;
    }

    /**  
     *  Gets the current connection 
     *  @return      The Connection
     **/
    public Connection getConn() {
        return this.conn; 
    }

    // Custom Methods! 

    /**
     * Attempts to log in to the database using log in credentials.
     * 
     * @param user  User object that will used to get username and password details. 
     */
    public void login(User user) throws SQLException {
        this.currentUser = null;
        this.conn = DriverManager.getConnection(this.url, user.getUsername(), user.getPassword());
        this.currentUser = user; 
        // isLive = true;
    }
}
