package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Warehouse implements SQLData {
    private static final String TYPE_NAME="WARTYP";
    private String warehouseName;
    private String warehouseAddress;
    

    public Warehouse(String warehouseName, String warehouseAddress){
        this.warehouseName = warehouseName; 
        this.warehouseAddress = warehouseAddress;
    }



    public String getWarehouseName() {
        return warehouseName;
    }
    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }
    public String getWarehouseAddress() {
        return warehouseAddress;
    }
    public void setWarehouseAddress(String warehouseAddress) {
        this.warehouseAddress = warehouseAddress;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return Warehouse.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setWarehouseName(stream.readString());
        setWarehouseAddress(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getWarehouseName());
        stream.writeString(getWarehouseAddress());
    }

     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Warehouse")); 
        String sql = "{call Super_Store.INSERT_WAREHOUSE(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }



}
