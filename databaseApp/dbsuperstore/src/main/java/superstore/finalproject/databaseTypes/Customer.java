package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Customer implements SQLData {
    private static final String TYPE_NAME="CUSTYP";
    private String customerFn;
    private String customerLn;
    private String customerEmail;
    private String customerAddress;

    public Customer(String fn, String ln, String email, String address){
        this.customerFn = fn; 
        this.customerLn = ln;
        this.customerEmail = email;
        this.customerAddress = address;
    }
    
    public String getCustomerFn() {
        return customerFn;
    }
    public void setCustomerFn(String customerFn) {
        this.customerFn = customerFn;
    }
    public String getCustomerLn() {
        return customerLn;
    }
    public void setCustomerLn(String customerLn) {
        this.customerLn = customerLn;
    }
    public String getCustomerEmail() {
        return customerEmail;
    }
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }
    public String getCustomerAddress() {
        return customerAddress;
    }
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }
    
    @Override
    public String getSQLTypeName() throws SQLException {
        return Customer.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setCustomerFn(stream.readString());
        setCustomerLn(stream.readString());
        setCustomerEmail(stream.readString());
        setCustomerAddress(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getCustomerFn());
        stream.writeString(getCustomerLn());
        stream.writeString(getCustomerEmail());
        stream.writeString(getCustomerAddress());
    }


     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Product")); 
        String sql = "{call Super_Store.INSERT_CUSTOMER(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }

}
