package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class ProductStore implements SQLData {
    private static final String TYPE_NAME="PSTYP";
    private int storeId;
    private int productId;


    public ProductStore(int storeId, int productId){
        this.storeId = storeId;
        this.productId = productId; 
    }


    public int getStoreId() {
        return storeId;
    }
    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return ProductStore.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setStoreId(stream.readInt());
        setProductId(stream.readInt());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getStoreId());
        stream.writeInt(getProductId());
    }


     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.ProductStore")); 
        String sql = "{call Super_Store.INSERT_PRODUCT_STORE(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }


}
