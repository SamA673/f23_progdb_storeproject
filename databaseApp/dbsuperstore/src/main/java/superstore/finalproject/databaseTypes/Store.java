package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Store implements SQLData {
    private static final String TYPE_NAME="STOTYP";
    private String storeName;

    public Store(String storeName){
        this.storeName = storeName; 
    }



    public String getStoreName() {
        return storeName;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return Store.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setStoreName(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getStoreName());
    }    



     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Store")); 
        String sql = "{call Super_Store.INSERT_STORE(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }


}
