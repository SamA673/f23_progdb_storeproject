package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Product implements SQLData {
    private static final String TYPE_NAME="PROTYP";
    private String productName;
    private String productCategory;
    private double price;

    public Product(String productName, String productCategory, double price){
        this.productName = productName; 
        this.productCategory = productCategory;
        this.price  = price; 
    }
    
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductCategory() {
        return productCategory;
    }
    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return Product.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductName(stream.readString());
        setProductCategory(stream.readString());
        setPrice(stream.readDouble());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getProductName());
        stream.writeString(getProductCategory());
        stream.writeDouble(getPrice());
    }

     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Product")); 
        String sql = "{call Super_Store.INSERT_PRODUCT(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }
}
