package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Inventory implements SQLData {
    public static final String TYPE_NAME="INVTYP";
    private int productId;
    private int warehouseId;
    private int quantity;
    
    public Inventory(int productId, int warehouseId, int quantity){
        this.productId = productId;
        this.warehouseId = warehouseId; 
        this.quantity = quantity;
    }


    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getWarehouseId() {
        return warehouseId;
    }
    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    @Override
    public String getSQLTypeName() throws SQLException {
        return Inventory.TYPE_NAME;
    }
    
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductId(stream.readInt());
        setWarehouseId(stream.readInt());
        setQuantity(stream.readInt());
    }
    
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getProductId());
        stream.writeInt(getWarehouseId());
        stream.writeInt(getQuantity());
    }


    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Inventory")); 
        String sql = "{call Super_Store.INSERT_INVENTORY(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }


}
