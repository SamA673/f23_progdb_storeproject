package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class OrdersProduct implements SQLData {
    private static final String TYPE_NAME="OPTYP";
    private int productId;
    private int orderId;
    private int quantity;
    

    public OrdersProduct(int productId, int orderId, int quantity){
        this.productId = productId;
        this.orderId = orderId;
        this.quantity = quantity; 
    }


    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return OrdersProduct.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductId(stream.readInt());
        setOrderId(stream.readInt());
        setQuantity(stream.readInt());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getProductId());
        stream.writeInt(getOrderId());
        stream.writeInt(getQuantity());
    }


     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.OrdersProduct")); 
        String sql = "{call Super_Store.INSERT_ORDERS_PRODUCT(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }


}
