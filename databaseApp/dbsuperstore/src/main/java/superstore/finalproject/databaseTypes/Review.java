package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Review implements SQLData {
    public static final String TYPE_NAME="REVTYP";
    private int productId;
    private int customerId;
    private int reviewFlag;
    private int productReview;
    private String reviewDescription;


    public Review(int productId, int customerId, int productReview, String reviewDescription){
        this.productId = productId;
        this.customerId = customerId;
        this.reviewFlag = 0; 
        this.productReview = productReview;
        this.reviewDescription = reviewDescription; 
    }
    
    public int getProductId() {
        return productId;
    }
    public void setProductId(int productId) {
        this.productId = productId;
    }
    public int getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    public int getReviewFlag() {
        return reviewFlag;
    }
    public void setReviewFlag(int reviewFlag) {
        this.reviewFlag = reviewFlag;
    }
    public int getProductReview() {
        return productReview;
    }
    public void setProductReview(int productReview) {
        this.productReview = productReview;
    }
    public String getReviewDescription() {
        return reviewDescription;
    }
    public void setReviewDescription(String reviewDescription) {
        this.reviewDescription = reviewDescription;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return Review.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setProductId(stream.readInt());
        setCustomerId(stream.readInt());
        setReviewFlag(stream.readInt());
        setProductReview(stream.readInt());
        setReviewDescription(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getProductId());
        stream.writeInt(getCustomerId());
        //stream.writeInt(getReviewFlag());
        stream.writeInt(getProductReview());
        stream.writeString(getReviewDescription());
    }


    public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Review")); 
        String sql = "{call Super_Store.INSERT_REVIEW(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }
}
