package superstore.finalproject.databaseTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Map;

public class Orders implements SQLData {
    private static final String TYPE_NAME="ORDTYP";
    private int customerId;
    private int storeId;
    private Date orderDate;

    public Orders(int customerId, int storeId, java.sql.Date orderDate){
        this.customerId = customerId;
        this.storeId = storeId;
        this.orderDate = orderDate; 
    }
    
    public int getCustomerId() {
        return customerId;
    }
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    public int getStoreId() {
        return storeId;
    }
    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }
    public Date getOrderDate() {
        return orderDate;
    }
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return Orders.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setCustomerId(stream.readInt());
        setStoreId(stream.readInt());
        setOrderDate(stream.readDate());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getCustomerId());
        stream.writeInt(getStoreId());
        stream.writeDate(getOrderDate());
    }



     public void addToDatabase(Connection conn) throws SQLException, ClassNotFoundException {
        Map map = conn.getTypeMap();
        conn.setTypeMap(map); 
        map.put(TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Orders")); 
        String sql = "{call Super_Store.INSERT_ORDERS(?)}";
        CallableStatement statement = conn.prepareCall(sql);
        statement.setObject(1, this); 
        statement.execute(); 
    }



}
