package superstore.finalproject.databaseTypes;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class AuditTable implements SQLData {
    private static final String TYPE_NAME="AUDTYP";
    private int userId;
    private String tableName;
    private String columnName;
    private String operation;
    private String oldValue;
    private String newValue;
    
    
    public String getTableName() {
        return tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getColumnName() {
        return columnName;
    }
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    public String getOperation() {
        return operation;
    }
    public void setOperation(String operation) {
        this.operation = operation;
    }
    public String getOldValue() {
        return oldValue;
    }
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }
    public String getNewValue() {
        return newValue;
    }
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }
    public int getUserId() {
        return userId;
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return AuditTable.TYPE_NAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setUserId(stream.readInt());
        setTableName(stream.readString());
        setColumnName(stream.readString());
        setOperation(stream.readString());
        setOldValue(stream.readString());
        setNewValue(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getUserId());
        stream.writeString(getTableName());
        stream.writeString(getColumnName());
        stream.writeString(getOperation());
        stream.writeString(getOldValue());
        stream.writeString(getNewValue());
    }
}
