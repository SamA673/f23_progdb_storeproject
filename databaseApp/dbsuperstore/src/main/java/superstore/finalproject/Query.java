package superstore.finalproject;

import java.util.Scanner;
import superstore.finalproject.databaseTypes.*;
import oracle.jdbc.OracleTypes;
import java.sql.*;

import superstore.finalproject.utils.DatabaseInteractions;
import superstore.finalproject.utils.UserInputInteraction;


/**
 * The `Query` class provides methods for executing queries on the Superstore database.
 * It includes methods for printing information related to products, warehouse quantities,
 * orders, audit logs, flagged reviews, and problematic users.
 * 
 * @author Sam A/David P
 * @version 1.0
 * @since 2023-10-21
 */


public class Query {

    /**
     * Prints information about products, filtered by category if specified.
     *
     * @param scan    Scanner for user input.
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printProducts(Scanner scan, Session session) throws SQLException {
        String category = Input.getStringInput("Any specific category? (Leave blank and press enter if not)", scan);
        CallableStatement callableStatement = session.getConn()
                .prepareCall("{call Super_Store.get_productsEnhanced(?, ?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.setString(2, category);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
        Display.flushScreen();
        System.out.println("ID    Name                           Category         Price      ");
        System.out.println("---------------------------------------------------------------------------");
        while (resultSet.next()) {

            String pId = "" + resultSet.getInt("ProductId");
            String pName = resultSet.getString("ProductName");
            String pCat = resultSet.getString("ProductCategory");
            String pr = "" + resultSet.getDouble("Price");
            System.out.println(String.format("%-5s %-30s %-16s %-10s", pId, pName, pCat, pr));
        }

        System.out.println();
    }



    /**
     * Prints the total quantity of each product across all warehouses.
     *
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printQuantityWarehouse(Session session) throws SQLException {
        CallableStatement callableStatement = session.getConn()
                .prepareCall("{call Super_Store.get_total_quantity_at_warehouses(?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
        Display.flushScreen();
        System.out.println("Product                                                  Total Quantity");
        System.out.println("---------------------------------------------------------------------------");

        while (resultSet.next()) {
            String ProductId = "" + resultSet.getInt("ProductId");
            String ProductName = resultSet.getString("ProductName");
            String TotalAvailable = "" + resultSet.getInt("TotalAvailable");
            System.out.println(String.format("%-3s %-52s %-3s", ProductId, ProductName, TotalAvailable));
        }
    }

    /**
     * Prints information about orders, filtered by customer ID, date, and store ID if specified.
     *
     * @param scan    Scanner for user input.
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printOrders(Scanner scan, Session session) throws SQLException {
        String response = "";
        int customerId = -1;
        int storeId = -1;
        java.sql.Date date = null;

        response = Input.getStringInput("Do you wanna input a specific CustomerId to narrow your search? (y) or (n)",
                scan);

        if (response.equals("y")) {
            customerId = Input.getIntInput("Input your customerId", scan);
        }

        response = Input.getStringInput("Do you wanna input a specific ORDER DATE to narrow your search? (y) or (n)",
                scan);
        if (response.equals("y")) {
            date = Input.getDateInput(scan);
        }

        response = Input.getStringInput("Do you wanna input a specific store id? to narrow your search? (y) or (n)",
                scan);
        if (response.equals("y")) {
            storeId = Input.getIntInput("Enter your StoreID", scan);
        }

        CallableStatement callableStatement = session.getConn()
                .prepareCall("{call Super_Store.get_orders(?, ?, ?, ?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.setInt(2, customerId);
        callableStatement.setDate(3, date);
        callableStatement.setInt(4, storeId);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
        Display.flushScreen();
        System.out.println("OrderID    CustomerID                        StoreID          DATE");
        System.out.println("---------------------------------------------------------------------------");
        while (resultSet.next()) {
            String OrderId = "" + resultSet.getInt("OrderId");
            String CustomerId = "" + resultSet.getInt("CustomerId");
            String StoreId = "" + resultSet.getInt("StoreId");
            String ordDate = "" + resultSet.getDate("OrderDate");
            System.out.println(String.format("%-11s %-34s %-14s %-12s", OrderId, CustomerId, StoreId, ordDate));
        }

        System.out.println();
    }



    /**
     * Prints the audit log of database operations.
     *
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printAuditLog(Session session) throws SQLException {
        CallableStatement callableStatement = session.getConn().prepareCall("{call Super_Store.get_auditlog(?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
        Display.bigFlush();
        System.out.println(
                "ID         Time         User       Table           Column               Operation  OldValue                                           NewValue");
        System.out.println(
                "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        while (resultSet.next()) {
            String auditId = "" + resultSet.getInt("AuditId");
            String date = "" + resultSet.getDate("AuditTime");
            String userId = "" + resultSet.getString("UserId");
            String tableName = "" + resultSet.getString("TableName");
            String columnName = "" + resultSet.getString("ColumnName");
            String operation = "" + resultSet.getString("Operation");
            String oldValue = "" + resultSet.getString("OldValue");
            String first50OldValue = oldValue.substring(0, Math.min(oldValue.length(), 50));
            String newValue = "" + resultSet.getString("NewValue");
            String first50NewValue = newValue.substring(0, Math.min(newValue.length(), 100));
            System.out.println(String.format("%-10s %-12s %-10s %-15s %-20s %-10s %-50s %-100s", auditId, date, userId,
                    tableName, columnName, operation, first50OldValue, first50NewValue));
        }
    }


    /**
     * Prints flagged reviews, including review ID, product ID, customer ID, flags, and review description.
     *
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printFlaggedReviews(Session session)throws SQLException{
        CallableStatement callableStatement = session.getConn().prepareCall("{call Super_Store.get_flaggedReviews(?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet)callableStatement.getObject(1);
        Display.flushScreen();
        System.out.println("ReviewID   ProductID   CustomerID   Flags Review Description");
        System.out.println("---------------------------------------------------------------------------");

        while(resultSet.next()){
            String rId = ""+resultSet.getInt("ReviewId");
            String pId = ""+resultSet.getInt("ProductId"); 
            String cId = ""+resultSet.getInt("CustomerId"); 
            String flag = ""+resultSet.getInt("ReviewFlag");
            String review = ""+resultSet.getInt("ProductReview");
            String desc = resultSet.getString("ReviewDescription");
            System.out.println(String.format("%-10s %-11s %-12s %-5s %-6s %-50s", rId, pId, cId, flag, review, desc));
        }
    }


    /**
     * Prints information about problematic users, including customer ID, name, and the number of problematic reviews.
     *
     * @param session The database session.
     * @throws SQLException If a database access error occurs.
     */
    public static void printProblemUsers(Session session) throws SQLException{
        CallableStatement callableStatement = session.getConn().prepareCall("{call Super_Store.get_flaggedCustomers(?)}");
        callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
        callableStatement.execute();
        ResultSet resultSet = (ResultSet)callableStatement.getObject(1);
        Display.flushScreen();
        System.out.println("ID    Name                                         Problematic Reviews");
        System.out.println("---------------------------------------------------------------------------");

        while(resultSet.next()){
            String cId = ""+resultSet.getInt("CustomerId");
            String fName = resultSet.getString("FirstName"); 
            String lName = resultSet.getString("LastName"); 
            String problems = ""+resultSet.getInt("TotalProblematicReviews"); 
            System.out.println(String.format("%-5s %-44s %-5s", cId, fName + " " +  lName, problems));
        }
    }

}