package superstore.finalproject.utils;

import java.util.Scanner;
import java.io.Console;

/**
 * Class for all user input interactions
 */
public class UserInputInteraction {
    /**
     * Asks the user for database username
    * @return A String containing the user username
    */
    public static String getUsername() {
        Scanner reader = new Scanner(System.in);
        System.out.println("<SYSTEM> Username:");
        return reader.nextLine();
    }
    
    /**
     * Asks t he user for database password
    * @return A String containing the user password
    */
    public static String getPassword() {
        Console console = System.console();
        char[] passwordArray = console.readPassword("<SYSTEM> Password:\n");
        return new String(passwordArray);
    }
}
