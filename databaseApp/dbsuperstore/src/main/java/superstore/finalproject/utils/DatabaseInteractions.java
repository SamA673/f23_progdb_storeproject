package superstore.finalproject.utils;

import java.sql.*;
import java.util.Map;
import java.util.Scanner;
import superstore.finalproject.Input;
import superstore.finalproject.databaseTypes.*;




/**
 * The `DatabaseInteractions` class provides utility methods for interacting with the Superstore database.
 * It includes methods for updating warehouse quantity, reviews, product information, and retrieving average reviews.
 * 
 * @author David P
 * @version 1.0
 * @since 2023-10-20
 */
public class DatabaseInteractions {



    /**
     * Updates the quantity of a product in the warehouse.
     *
     * @param inv  The inventory object containing information about the warehouse and product.
     * @param conn The database connection.
     */    
    public static void updateWarehouseQuantity(Inventory inv, Connection conn) {
        try {
            Map map = conn.getTypeMap();
            conn.setTypeMap(map);
            map.put(Inventory.TYPE_NAME, Class.forName("superstore.finalproject.databaseTypes.Inventory"));
            String sql = "{ call Super_Store.update_warehouse_quantity(?) }";
            try(CallableStatement stmt = conn.prepareCall(sql)) {
                stmt.setObject(1, inv);
                stmt.execute();
                System.out.println("Successful update!\n");
            } catch (SQLException e) {
                System.out.println("<SYSTEM> Something went wrong.");
                String[] msg = e.getMessage().split("\n");
                System.out.println(msg[0] + "\n");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Updates a product review.
     *
     * @param reviewId    The ID of the review to be updated.
     * @param reviewScore The new review score.
     * @param review      The new review text.
     * @param conn        The database connection.
     */
    public static void updateReview(int reviewId, int reviewFlag, int reviewScore, String review, Connection conn) {
        String sql = "{ call Super_Store.update_review(?, ?, ?, ?) }";
        try(CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, reviewId);
            stmt.setInt(2, reviewFlag);
            stmt.setInt(3, reviewScore);
            stmt.setString(4, review);
            stmt.execute();
            System.out.println("Successful update!\n");
        } catch (SQLException e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }


    /**
     * Updates a flagged product review.
     *
     * @param reviewId    The ID of the review to be updated.
     * @param reviewScore The new review score.
     * @param review      The new review text.
     * @param conn        The database connection.
     */
    public static void updateFlaggedReview(int reviewId, int reviewFlag, int reviewScore, String review, Connection conn) {
        String sql = "{ call Super_Store.update_flagged_review(?, ?, ?, ?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, reviewId);
            stmt.setInt(2, reviewFlag);
            stmt.setInt(3, reviewScore);
            stmt.setString(4, review);
            stmt.execute();
            System.out.println("Successful update!\n");
        } catch (SQLException e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    /**
     * Updates the category of a product.
     *
     * @param productId The ID of the product to be updated.
     * @param category  The new category of the product.
     * @param conn      The database connection.
     */
    public static void updateProductCategory(int productId, String category, Connection conn) {
        String sql = "{ call Super_Store.update_product_category(?, ?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, productId);
            stmt.setString(2, category);
            stmt.execute();
            System.out.println("Successful update!\n");
        } catch (SQLException e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    /**
     * Updates the name of a product.
     *
     * @param productId The ID of the product to be updated.
     * @param name      The new name of the product.
     * @param conn      The database connection.
     */
    public static void updateProductName(int productId, String name, Connection conn) {
        String sql = "{ call Super_Store.update_product_name(?, ?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, productId);
            stmt.setString(2, name);
            stmt.execute();
            System.out.println("Successful update!\n");
        } catch (SQLException e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }


    /**
     * Retrieves and prints the average review score for a product.
     *
     * @param productId The ID of the product.
     * @param conn      The database connection.
     */
    public static void getAverageReview(int productId, Connection conn) {
        String sql = "{ ? = call Super_Store.get_average_review(?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.registerOutParameter(1, Types.DECIMAL);
            stmt.setInt(2, productId);
            stmt.execute();
            String averageReview = String.format("%.2f", stmt.getDouble(1));
            System.out.println("Average Review");
            System.out.println("---------------------------------------------------------------------------");
            System.out.println(averageReview);
        } catch (SQLException e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    public static void deleteOrder(int orderId, Connection conn) {
        String sql = "{ call Super_Store.delete_order(?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, orderId);
            stmt.execute();
            System.out.println("Successful delete!\n");
        } catch (Exception e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    public static void deleteReview(int reviewId, Connection conn) {
        String sql = "{ call Super_Store.delete_review(?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, reviewId);
            stmt.execute();
            System.out.println("Successful delete!\n");
        } catch (Exception e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    public static void deleteFlaggedReview(int reviewId, Connection conn) {
        String sql = "{ call Super_Store.delete_flagged_review(?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, reviewId);
            stmt.execute();
            System.out.println("Successful delete!\n");
        } catch (Exception e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }

    public static void deleteWarehouse(int warehouseId, Connection conn) {
        String sql = "{ call Super_Store.delete_warehouse(?) }";
        try (CallableStatement stmt = conn.prepareCall(sql)) {
            stmt.setInt(1, warehouseId);
            stmt.execute();
            System.out.println("Successful delete!\n");
        } catch (Exception e) {
            System.out.println("<SYSTEM> Something went wrong.");
            String[] msg = e.getMessage().split("\n");
            System.out.println(msg[0] + "\n");
        }
    }
}

