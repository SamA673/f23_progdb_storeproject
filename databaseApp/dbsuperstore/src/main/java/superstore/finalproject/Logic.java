package superstore.finalproject;
import java.sql.*;


/**
 * The `Logic` class contains logic-related methods for the Superstore database system.
 * It includes methods for user authentication and other future business logic. 
 * 
 * @author Sam A
 * @version 1.0
 * @since 2023-10-20
 */


public class Logic{
    /**
     * Authenticates a user by attempting to log in with the provided username and password.
     * 
     * @param session  The database session.
     * @param username The username of the user attempting to log in.
     * @param password The password of the user attempting to log in.
     * @throws SQLException If a database access error occurs.
     */
    public static void login(Session session, String username, String password) throws SQLException{ 
        User user = new User(username, password); 
        session.login(user);
    }
}
