package superstore.finalproject;



/**
 * The `Display` class provides methods for displaying information and menus in the Superstore database client.
 * It includes methods for success messages, user greetings, menus for adding, updating, displaying, and moderating,
 * as well as methods for flushing the screen and handling not-yet-implemented features.
 * 
 * @author Sam A 
 * @since 2023-11-01
 */
public class Display {


    /**
     * Prints a success message to the console.
     */
    public static void success() {
        System.out.println("Success!");
    }


    /**
     * Returns a menu for adding various entities to the Superstore database.
     *
     * @return The menu for adding entities.
     */
    public static String returnAddMenu() {
        String menu = ""; 
        menu += "What would you like to add?" + '\n'; 
        menu += "1. Product"+ '\n'; 
        menu += "2. Customer"+ '\n'; 
        menu += "3. Store"+ '\n'; 
        menu += "4. Warehouse"+ '\n'; 
        menu += "5. Orders"+ '\n'; 
        menu += "6. Review"+ '\n'; 
        menu += "7. Inventory"+ '\n'; 
        menu += "8. ProductStore"+ '\n'; 
        menu += "9. OrdersProduct"+ '\n'; 
        menu += "0. I'm done adding!" + '\n';
        return menu; 
    }



    /**
     * Generates a greeting message for the admin user.
     *
     * @param user The username of the admin.
     * @return The greeting message for the admin user.
     */
    public static String greetAdmin(String user) {
        String greeter = "Hi Admin " + user + '\n'; 
        greeter += "What would you like to do today?" +'\n';
        greeter += "1: Display" +'\n'; 
        greeter += "2: Add" + '\n';
        greeter += "3: Update" + '\n';
        greeter += "4: Delete" + '\n';
        greeter += "5: Moderate Reviews" + '\n';
        greeter += "0: Quit" + '\n';
        return greeter; 
    }


    /**
     * Returns a menu for displaying different types of information in the Superstore database.
     *
     * @return The menu for displaying information.
     */
        public static String returnDisplayMenu() {
        String menu = "What do you like to display?" +'\n';
        menu += "1: Display products (All or by category)" +'\n'; 
        menu += "2: Display total quantity of a product across warehouses" + '\n';
        menu += "3: Display orders (All or filtered by CustomerId, Date, Store)" + '\n';
        menu += "4: Display audit log" + '\n';
        menu += "5: Display average review of a product" + '\n';
        menu += "0: I'm done displaying!" + '\n';
        return menu; 
    }


    /**
     * Clears the console screen and displays a header for the Superstore database client.
     */
    public static void flushScreen() { 
        System.out.print("\033[H\033[2J");  
        System.out.flush();   
        System.out.println("--------------------------- DATABASE CLIENT 1.0 ---------------------------");
    }


    /**
     * Prints a message indicating that a feature is not yet developed.
     */
    public static void notDevelopped() {
        System.out.println("Not yet developped");
    }


    /**
     * Returns a menu for updating various entities in the Superstore database.
     *
     * @return The menu for updating entities.
     */
    public static String updateMenu() {
        String menu = "";
        menu += "What would you like to update?" + '\n'; 
        menu += "1. Warehouse Product Inventory Quantity"+ '\n'; 
        menu += "2. Reviews with 1 flag and under"+ '\n'; 
        menu += "3. Product Info (Category or Name)"+ '\n'; 
        menu += "0: I'm done updating!" + '\n';
        return menu;
    }


    /**
     * Returns a menu for updating product information in the Superstore database.
     *
     * @return The menu for updating product information.
     */
    public static String updateProductMenu() {
        String menu = "";
        menu += "How would you like to update the product?" + '\n';
        menu += "1. Category"+ '\n'; 
        menu += "2. Product Name"+ '\n'; 
        menu += "0. I'm done updating product info!" + '\n';
        return menu;
    }

    /**
     * Returns a menu for moderating reviews in the Superstore database.
     *
     * @return The menu for moderating reviews.
     */
    public static String returnReviewMenu(){
        String menu = "";
        menu += "What would you like to do? " + '\n';
        menu += "1. Check flagged reviews"+ '\n'; 
        menu += "2. Check problematic customers"+ '\n'; 
        menu += "3. Update overly flagged reviews" + '\n';
        menu += "4. Delete overly flagged reviews" + '\n';
        menu+= "0. I'm done moderating!" + '\n'; 
        return menu ;
    }

    public static String deleteMenu() {
        String menu = "";
        menu += "What would you like to delete? " + '\n';
        menu += "1. Order"+ '\n'; 
        menu += "2. Reviews with 1 flag and under"+ '\n'; 
        menu += "3. Warehouse" + '\n';
        menu += "0. I'm done deleting!" + '\n';
        return menu;
    }


    /**
     * Clears the console screen and displays a large header for the Superstore database client.
     */
    public static void bigFlush(){
        System.out.print("\033[H\033[2J");  
        System.out.flush();   
        System.out.println("-------------------------------------------------------------------------------------------------------- DATABASE CLIENT 1.0 --------------------------------------------------------------------------------------------------------");
    }

}

