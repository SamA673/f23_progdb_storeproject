package superstore.finalproject;

/**
 * The `User` class represents a user in the Superstore database system.
 * It includes the user's username and password.
 * 
 * @author Sam A
 * @version 1.0
 * @since 2023-10-15
 */
public class User {

    private String username; 
    private String password;

    /**
     * Constructs a new User instance with the specified username and password.
     *
     * @param username The username of the user.
     * @param password The password of the user.
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Gets the username of the user.
     *
     * @return The username of the user.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the password of the user.
     *
     * @return The password of the user.
     */
    public String getPassword() {
        return password;
    }
}
