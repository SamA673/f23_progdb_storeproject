package superstore.finalproject;

import java.util.Scanner;
import superstore.finalproject.databaseTypes.*;
import oracle.jdbc.OracleTypes;
import java.sql.*;

import superstore.finalproject.utils.DatabaseInteractions;
import superstore.finalproject.utils.UserInputInteraction;

/**
 * The `App` class is the main entry point for the Superstore database application.
 * It provides the main method to run the application, handle user authentication, and
 * execute different functionalities for administrators and regular users.
 * 
 * @author David P / Sam A 
 * @version 1.0 
 * @since 2023-10-15
 */
public class App {


    /**
     * The main entry point for the Superstore database application.
     */
    public static void main(String[] args) {
        final String URL = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        final String adminPW = "123"; 
        final int PW_TRY_COUNT = 3; 
        runDatabase(URL);
    }

    /**
     * This runs the Superstore database application.
     *
     * @param URL The URL of the database.
     */
    public static void runDatabase(String URL){
        Scanner scan = new Scanner(System.in); 

        // Create the session with URL
        Session session = new Session(URL); 
        String stringInput; 
        boolean isAdmin = false; 

        // While there is no active user in 'Session', ask the user for details until a
        // valid log in is provided.
        while (session.getCurrentUser() == null) {
            Display.flushScreen();
            String username = UserInputInteraction.getUsername();  
            String password = UserInputInteraction.getPassword();   
            try{
                Logic.login(session, username, password); 
            }catch(SQLException e){
                System.out.println("<SYSTEM> Log in unsuccessful... Try again!"); 
                Input.waitForContinue(scan);
            }
        }

        Display.flushScreen();

        stringInput = "y";                                                                             
        if (stringInput.equals("y")){
            isAdmin = true; 
        }

        if (isAdmin) {
            runAdminGUI(session, scan);
        } else {
            runUserGUI(session, scan);
        }
    }

    /**
     * Runs the administrator interface for the Superstore database.
     *
     * @param session The database session.
     * @param scan    The Scanner object for user input.
     */
    public static void runAdminGUI(Session session, Scanner scan) {
        int input;
        boolean isValid = false;
        boolean addValid = false;

        while (!isValid) {
            isValid = false;
            addValid = false;
            input = -1;

            Display.flushScreen();
            Display.greetAdmin(session.getCurrentUser().getUsername());
            input = Input.getIntInput(Display.greetAdmin(session.getCurrentUser().getUsername()), scan);

            // Displaying
            if (input == 1) {
                input = -1;
                addValid = false;
                while (!addValid) {
                    Display.flushScreen();
                try{
                    input = Input.getIntInput(Display.returnDisplayMenu(), scan);
                    // Products (All / Category)
                    if (input == 1){
                        Query.printProducts(scan, session);
                        Input.waitForContinue(scan);

                    // Display quantity of products across warehouses
                    } else if (input == 2){
                            Query.printQuantityWarehouse(session);
                            Input.waitForContinue(scan);

                    // Display orders (All / CustomerId / Date / StoreId)
                    } else if (input == 3){
                        Query.printOrders(scan, session); 
                        Input.waitForContinue(scan);

                    // Print audit log 
                    } else if( input == 4){
                        Query.printAuditLog(session); 
                        Input.waitForContinue(scan);    
                    } else if( input == 5){
                        int productId = Input.getIntInput("Enter a product id:", scan);
                        DatabaseInteractions.getAverageReview(productId, session.getConn());
                        Input.waitForContinue(scan);
                    } else if (input == 0){
                        addValid = true; 
                    }
                } catch (SQLException e){
                    System.out.println("<SYSTEM> Something went wrong. ");
                    String[] msg = e.getMessage().split("\n"); 
                    System.out.println(msg[0]); 
                    Input.waitForContinue(scan);
                } 
            }

                // Inserting
            } else if (input == 2) {
                addValid = false;
                while (!addValid) {
                    input = -1;
                    Display.flushScreen();
                    input = Input.getIntInput(Display.returnAddMenu(), scan);

                    try {
                        // Product
                        if (input == 1) {
                            Product p = Input.createProduct(scan);
                            p.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Customer
                        } else if (input == 2) {
                            Customer c = Input.createCustomer(scan);
                            c.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Store
                        } else if (input == 3) {
                            Store s = Input.createStore(scan);
                            s.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Warehouse
                        } else if (input == 4) {
                            Warehouse s = Input.createWarehouse(scan);
                            s.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Order ?
                        } else if (input == 5) {
                            Orders s = Input.createOrders(scan);
                            s.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Review?
                        } else if (input == 6) {
                            Review i = Input.createReview(scan);
                            i.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Inv?
                        } else if (input == 7) {
                            Inventory i = Input.createInventory(scan);
                            i.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // ProductStore ?
                        } else if (input == 8) {
                            ProductStore ps = Input.createProductStore(scan);
                            ps.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);

                            // Orders Product ?
                        } else if (input == 9) {
                            OrdersProduct i = Input.createOrdersProduct(scan);
                            i.addToDatabase(session.getConn());
                            Display.success();
                            Input.waitForContinue(scan);
                            // Done
                        } else if (input == 0) {
                            addValid = true;
                        }
                    } catch(SQLException e){
                        System.out.println("<SYSTEM> Something went wrong. ");
                        String[] msg = e.getMessage().split("\n"); 
                        System.out.println(msg[0]); 
                        Input.waitForContinue(scan);
                    } catch (ClassNotFoundException c) {
                        System.out
                                .println("<SYSTEM> There was a problem locating the class used for adding your entry.");
                        Input.waitForContinue(scan);
                    } catch (NumberFormatException g) {
                        System.out.println("<SYSTEM> Input a number (integer), not a letter or letters.");
                        Input.waitForContinue(scan);
                    }
                }

                // Update (David WIP)
            } else if (input == 3) {
                addValid = false;
                while (!addValid) {
                    Display.flushScreen();
                    switch (Input.getIntInput(Display.updateMenu(), scan)) {
                        case 1:
                            Inventory inv = Input.createInventory(scan);
                            DatabaseInteractions.updateWarehouseQuantity(inv, session.getConn());
                            Input.waitForContinue(scan);
                            break;
                        case 2:
                            int reviewId = Input.getIntInput("Enter a review id:", scan);
                            int reviewFlag = Input.getIntInput("Enter the amount of flags:", scan);
                            int reviewScore = Input.getIntInput("Enter a review score(0-5):", scan);
                            String review = Input.getStringInput("Enter a review:", scan);
                            DatabaseInteractions.updateReview(reviewId, reviewFlag, reviewScore, review, session.getConn());
                            Input.waitForContinue(scan);
                            break;
                        case 3:
                            Display.flushScreen();
                            boolean prodUpdateValid = false;
                            while (!prodUpdateValid) {
                                Display.flushScreen();
                                switch (Input.getIntInput(Display.updateProductMenu(), scan)) {
                                    case 1:
                                        int productId = Input.getIntInput("Enter a product id:", scan);
                                        String category = Input.getStringInput("Enter a category:", scan);
                                        DatabaseInteractions.updateProductCategory(productId, category,
                                                session.getConn());
                                        Input.waitForContinue(scan);
                                        break;
                                    case 2:
                                        productId = Input.getIntInput("Enter a product id:", scan);
                                        String name = Input.getStringInput("Enter a product name:", scan);
                                        DatabaseInteractions.updateProductName(productId, name, session.getConn());
                                        Input.waitForContinue(scan);
                                        break;
                                    case 0:
                                        prodUpdateValid = true;
                                        break;
                                    default:
                                        System.out.println("<SYSTEM> Invalid input. Please try again.");
                                        Input.waitForContinue(scan);
                                        break;
                                }
                            }
                            break;
                        case 0:
                            addValid = true;
                            break;
                        default:
                            System.out.println("<SYSTEM> Invalid input. Please try again.");
                            Input.waitForContinue(scan);
                            break;
                    }
                }

                // Delete (DAVID WIP)
            } else if (input == 4) {
                addValid = false;
                while(!addValid) {
                    Display.flushScreen();
                    switch (Input.getIntInput(Display.deleteMenu(), scan)) {
                        case 1:
                            int orderId = Input.getIntInput("Enter an order id:", scan);
                            DatabaseInteractions.deleteOrder(orderId, session.getConn());
                            Input.waitForContinue(scan);
                            break;
                        case 2:
                            int reviewId = Input.getIntInput("Enter a review id:", scan);
                            DatabaseInteractions.deleteReview(reviewId, session.getConn());
                            Input.waitForContinue(scan);
                            break;
                        case 3:
                            int warehouseId = Input.getIntInput("Enter a warehouse id:", scan);
                            DatabaseInteractions.deleteWarehouse(warehouseId, session.getConn());
                            Input.waitForContinue(scan);
                            break;
                        case 0:
                            addValid = true;
                            break;
                        default:
                            System.out.println("<SYSTEM> Invalid input. Please try again.");
                            Input.waitForContinue(scan);
                            break;
                    }
                }

            // Moderate reviews 
            }else if(input == 5){
                addValid = false; 
                while(!addValid){
                    input = -1;
                    Display.flushScreen();
                    input = Input.getIntInput(Display.returnReviewMenu(), scan); 

                    try{
                        // Check Flagged reviews 
                        if(input == 1){    
                            Query.printFlaggedReviews(session);
                            Input.waitForContinue(scan);

                        // Check Flagged Problematic Customers
                        }else if(input == 2) {   
                            Query.printProblemUsers(session);
                            Input.waitForContinue(scan);

                        // Update flagged reviews
                        }else if(input == 3) { 
                            int reviewId = Input.getIntInput("Enter a review id:", scan);
                            int reviewFlag = Input.getIntInput("Enter the amount of flags:", scan);
                            int reviewScore = Input.getIntInput("Enter a review score (0-5):", scan);
                            String review = Input.getStringInput("Enter a review: ", scan);
                            DatabaseInteractions.updateFlaggedReview(reviewId, reviewFlag, reviewScore, review, session.getConn());
                            Input.waitForContinue(scan);
                        // Delete flagged reviews 
                        }else if(input == 4) {    
                            int reviewId = Input.getIntInput("Enter a review id:", scan);
                            DatabaseInteractions.deleteFlaggedReview(reviewId, session.getConn());
                            Input.waitForContinue(scan);
                        // Go back
                        } else if(input == 0) {   
                            addValid = true;
                        }
                        
                    } catch(SQLException e){
                        System.out.println("<SYSTEM> Something went wrong. ");
                        String[] msg = e.getMessage().split("\n"); 
                        System.out.println(msg[0]); 
                        Input.waitForContinue(scan);
                    }catch(NumberFormatException g){
                        System.out.println("<SYSTEM> Input a number (integer), not a letter or letters.");
                        Input.waitForContinue(scan);
                    }
                }
            }else if(input == 0){
                System.out.println("Goodbye!"); 
                isValid = true; 
            }else{
                System.out.println("<SYSTEM> Please enter an integer from 0-4"); 
                Input.waitForContinue(scan);
            }
        }

    }

    /**
     * Runs the regular user interface for the Superstore database. 
     * Since the database currently has no use for a regular employee, 
     * this method requests the employee to contact an administrator, as they
     * personally do not have the right authorization to access the database. 
     *
     * @param session The database session.
     * @param scan    The Scanner object for user input.
     */
    public static void runUserGUI(Session session, Scanner scan) {
        System.out.println("<SYSTEM> ACCESS DENIED. CONTACT A DATABASE ADMINISTRATOR FOR HELP.");
    }
}
