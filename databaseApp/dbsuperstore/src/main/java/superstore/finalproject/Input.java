package superstore.finalproject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.Date;
import superstore.finalproject.databaseTypes.*;

/**
 * The Input class holds a range of helper methods that are related to user input.
 * @author Sam A / David P 
 * @since 2023-11-01
 */

public class Input {


    /**
     * Stops the program so that the user can resume when they want to.
     * @param scan  The necessary scanner for inputs.
     */
    public static void waitForContinue(Scanner scan) {
        System.out.println("<SYSTEM> Press enter to continue");
        scan.nextLine(); 
    }


    /**
     * Asks the what the password is and gives them n tries to guess. 
     * If guessed correctly within n tries, returns true, otherwise false.
     * @param scan  The necessary scanner for inputs.
     * @param password  The password the user should know
     * @param tries     The number of tries the user has before being kicked out 
     * @return boolean      true if the user is authenticated (authorized user)
     */
    public static boolean tryPW(Scanner scan, String password, int tries) {
            while(tries > 0){
                if (getStringInput("<SYSTEM> Password?", scan).equals(password)) {
                    return true; 
                }
                tries--; 
                System.out.println("<SYSTEM> Wrong. You have " + tries + " try/tries left."); 
            }
        return false; 
    }

    /**
     * Gets a string input from the user.
     * @param scan  Necessary scanner object
     * @param message   Message to be printed before asking the user to input something. 
     * @return String   User input.
     */
    public static String getStringInput(String message, Scanner scan) {
        System.out.println(message); 
        return scan.nextLine(); 
    }


    /**
     * Gets a double input from the user.
     * @param scan  Necessary scanner object
     * @param message   Message to be printed before asking the user to input something. 
     * @return double   User inout.
     */
    public static double getDoubleInput(String message, Scanner scan) {
        System.out.println(message);
        while(true){
            try{
                return Double.parseDouble(scan.nextLine());
            } catch (NumberFormatException e){
                System.out.println("<SYSTEM> Try again with a double (IE: 23.14, 1, 23.25)..."); 
            }
        }
    }

    /**
     * Gets an integer input from the user.
     * @param scan  Necessary scanner object
     * @param message   Message to be printed before asking the user to input something.
     * @return int      Inputted integer.  
     */
    public static int getIntInput(String message, Scanner scan) {
        System.out.println(message); 
        while(true) {
            try{
                return Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e){
                System.out.println("<SYSTEM> Please input an integer."); 
            }
        }
    }

    /**
     * Gets a date input from the user.
     * @param scan  Necessary scanner object.
     * @return Date The inputted date.
     */
    public static java.sql.Date getDateInput(Scanner scan){
        java.sql.Date date = null;
        boolean validInput = false;

        // Creates a data format so that we can parse the date to make sure it's the right format. 
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


        while (!validInput) {
            try {
                String userInput = getStringInput("<SYSTEM> YYYY-MM-DD", scan);
                Date parsedDate = dateFormat.parse(userInput);

                // Converting my java.util.Date to java.sql.Date
                date = new java.sql.Date(parsedDate.getTime());

                validInput = true; // Break out of the loop if parsing is successful
            } catch (ParseException e) {
                System.out.println("<SYSTEM> Invalid date format. Please enter the date in the format YYYY-MM-dd.");
            }
        }

        return date;
    }


    /**
     * Creates and returns a product. 
     * @param scan  Necessary scanner object
     * @return Product  Created by the user
     */
    public static Product createProduct(Scanner scan) {
        String name = getStringInput("Product Name?", scan);
        String category = getStringInput("Category?", scan);
        double price = getDoubleInput("Price?", scan);
        return new Product(name, category, price);
    }


     /**
     * Creates and returns a customer. 
     * @param scan  Necessary scanner object
     * @return Product  Created by the user
     */
    public static Customer createCustomer(Scanner scan) {
        String firstName = getStringInput("First Name", scan);
        String lastName = getStringInput("Last name?", scan);
        String email = getStringInput("Email?", scan);
        String address = getStringInput("Address?", scan);
        return new Customer(firstName, lastName, email, address); 
    }

    /**
     * Creates and returns a store. 
     * @param scan  Necessary scanner object
     * @return Store  Created by the user
     */
    public static Store createStore(Scanner scan) {
            String name = getStringInput("Store name?", scan);
            return new Store(name); 
        } 
    
    /**
     * Creates and returns a warehouse. 
     * @param scan  Necessary scanner object
     * @return Warehouse  Created by the user
     */
    public static Warehouse createWarehouse(Scanner scan) {
        String name = getStringInput("Warehouse name?", scan);
        String address = getStringInput("Address?", scan);
        return new Warehouse(name, address);  
    } 

    /**
     * Creates and returns a ProductStore row. 
     * @param scan  Necessary scanner object
     * @return ProductStore Created by the user
     */
    public static ProductStore createProductStore(Scanner scan) {
        int storeId = getIntInput("Store ID?", scan);
        int productId = getIntInput("ProducdID?", scan);
        return new ProductStore(storeId, productId); 
    }


    /**
     * Creates and returns a review. 
     * @param scan  Necessary scanner object
     * @return Review Created by the user
     */
    public static Review createReview(Scanner scan) {
        int productId = Input.getIntInput("ProductID?", scan);
        int customerId = Input.getIntInput("CustomerID?", scan);
        int productReview = Input.getIntInput("Product Review? (0-5) (-1 for no review)", scan);
        String reviewDescription = Input.getStringInput("Review Description?", scan);
        return new Review(productId, customerId, productReview, reviewDescription); 
    }


     /**
     * Creates and returns an order. 
     * @param scan  Necessary scanner object
     * @return Orders  Created by the user
     */
    public static Orders createOrders(Scanner scan) {
        int customerId = getIntInput("CustomerId?", scan);
        int storeId = getIntInput("StoreID?", scan);
        java.sql.Date orderDate = getDateInput(scan);
        return new Orders(customerId, storeId, orderDate); 
    }


    /**
     * Creates and returns an OrdersProduct row. 
     * @param scan  Necessary scanner object
     * @return OrdersProduct  Created by the user
     */
    public static OrdersProduct createOrdersProduct(Scanner scan) {
        int productId = getIntInput("ProductID?", scan);
        int orderId = getIntInput("OrderID", scan);
        int quantity = getIntInput("Quantity?", scan);
        return new OrdersProduct(productId,orderId, quantity); 
    }


    /**
     * Creates and returns an inventory row. 
     * @param scan  Necessary scanner object
     * @return Inventory  Created by the user
     */
    public static Inventory createInventory(Scanner scan) {
        int productId = getIntInput("ProductID?", scan);
        int warehouseId = getIntInput("WarehouseID?", scan);
        int quantity = getIntInput("Quantity?", scan);
        return new Inventory(productId, warehouseId, quantity);
    }
}







