-- Sam / David 
-- Friday Nov 3 2023
-- Last Edited: Nov 20 2023

----------------------------------------------------------------------------------------

-- Dropping Tables      
DROP TABLE AUDITTABLE; 
DROP TABLE PRODUCT_STORE; 
DROP TABLE Orders_Product;
DROP TABLE ORDERS;
DROP TABLE STORE;
DROP TABLE REVIEW;
DROP TABLE CUSTOMER;
DROP TABLE INVENTORY;
DROP TABLE PRODUCT; 
DROP TABLE WAREHOUSE; 

----------------------------------------------------------------------------------------
-- TABLE CREATION!


-- Create the Warehouse table!
CREATE TABLE Warehouse (
  WarehouseId           NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  WarehouseName         VARCHAR2(20)        NOT NULL,
  WarehouseAddress      VARCHAR2(100)
);

-- Create the Product table!
CREATE TABLE Product (
  ProductId             NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  ProductName           VARCHAR2(20)        NOT NULL,
  ProductCategory       VARCHAR2(20),
  Price                 NUMBER(10, 2),
  CONSTRAINT CHK_Price CHECK (Price > 0)
);

-- Create the Inventory table!
CREATE TABLE Inventory (
  ProductId             NUMBER      NOT NULL,
  WarehouseId           NUMBER      NOT NULL,
  Quantity              NUMBER      NOT NULL,
  FOREIGN KEY (ProductId)   REFERENCES Product(ProductId),
  FOREIGN KEY (WarehouseId) REFERENCES Warehouse(WarehouseId),
  CONSTRAINT CHK_IQuantity CHECK (Quantity >= 0)
);

-- Create the Customer table!
CREATE TABLE Customer (
  CustomerId            NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  CustomerFN            VARCHAR2(20)        NOT NULL,
  CustomerLN            VARCHAR2(20)        NOT NULL,
  CustomerEmail         VARCHAR2(50),
  CustomerAddress       VARCHAR2(50)
);

-- Create the Review table!
CREATE TABLE Review (
  ReviewId              NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  ProductId             NUMBER              NOT NULL,
  CustomerId            NUMBER              NOT NULL,
  ReviewFlag            NUMBER(6),       
  ProductReview         NUMBER(1),        
  ReviewDescription     VARCHAR2(150),
  FOREIGN KEY (ProductId)   REFERENCES Product(ProductId),
  FOREIGN KEY (CustomerId)  REFERENCES Customer(CustomerId),
  CONSTRAINT CHK_ProductReview CHECK ((ProductReview >= 0 AND ProductReview <= 5) OR (ProductReview IS NULL)),
  CONSTRAINT CHK_ReviewFlag CHECK (ReviewFlag >= 0)
);

-- Create the Store table!
CREATE TABLE Store (
    StoreId     NUMBER                      GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
    StoreName   VARCHAR2(20)                NOT NULL
);

-- Create the Order table!
CREATE TABLE Orders (
  OrderId               NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  CustomerId            NUMBER              NOT NULL,
  StoreId               NUMBER              NOT NULL,
  OrderDate             DATE,
  FOREIGN KEY (CustomerId)  REFERENCES Customer(CustomerId),
  FOREIGN KEY (StoreId)     REFERENCES Store(StoreId)
);

-- Create the   Orders_Product table!
CREATE TABLE Orders_Product (
  ProductId             NUMBER          NOT NULL,
  OrderId               NUMBER          NOT NULL,
  Quantity              NUMBER          NOT NULL,
  FOREIGN KEY (ProductId)   REFERENCES Product(ProductId),
  FOREIGN KEY (OrderId)     REFERENCES Orders(OrderId),
  CONSTRAINT CHK_OPQuantity CHECK (Quantity > 0)
);

-- Create the Product_Store table!
CREATE TABLE Product_Store (
    StoreId     NUMBER                  NOT NULL,
    ProductId   NUMBER                  NOT NULL,
    FOREIGN KEY (StoreId)   REFERENCES Store(StoreId),
    FOREIGN KEY (ProductId) REFERENCES Product(ProductId)
);

-- Create the Audit table
CREATE TABLE AuditTable (
  AuditId               NUMBER              GENERATED ALWAYS AS IDENTITY    PRIMARY KEY,
  AuditTime             DATE,
  UserId                VARCHAR2(10),
  TableName             VARCHAR2(50),
  ColumnName            VARCHAR2(50),
  Operation             VARCHAR2(15),
  OldValue              VARCHAR2(200),
  NewValue              VARCHAR2(200)
);

----------------------------------------------------------------------------------------
-- TRIGGER CREATION!

/**
    A trigger on the product table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER product_audit 
after insert or update or delete
ON product
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'ProductID: ' || TO_CHAR(:NEW.ProductId) || ', Product Name: ' || :NEW.ProductName || ', Product Category: ' || :NEW.ProductCategory || ', Price: ' || TO_CHAR(:NEW.Price);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Product', null, 'Insert', null, insertedData);
    END IF;

    IF UPDATING THEN
        IF :OLD.Price != :NEW.Price THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Product', 'Price', 'Update', TO_CHAR(:OLD.Price), TO_CHAR(:NEW.Price));
        END IF;
    END IF;
    
    IF DELETING THEN
        deletedData := 'ProductID: ' || TO_CHAR(:OLD.ProductId) || ', Product Name: ' || :OLD.ProductName || ', Product Category: ' || :OLD.ProductCategory || ', Price: ' || TO_CHAR(:OLD.Price);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Product', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the inventory table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER inventory_audit 
after insert or update or delete
ON inventory
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'ProductID: ' || TO_CHAR(:NEW.ProductId) || ', WarehouseId: ' || TO_CHAR(:NEW.WarehouseId) || ', Quantity: ' || TO_CHAR(:NEW.Quantity);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Inventory', null, 'Insert', null, insertedData);
    END IF;
    
    IF UPDATING THEN
        IF :OLD.Quantity != :NEW.Quantity THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Inventory', 'Quantity', 'Update', TO_CHAR(:OLD.Quantity), TO_CHAR(:NEW.Quantity));
        END IF;
    END IF;
    
    IF DELETING THEN
        deletedData := 'ProductID: ' || TO_CHAR(:OLD.ProductId) || ', WarehouseId: ' || TO_CHAR(:OLD.WarehouseId) || ', Quantity: ' || TO_CHAR(:OLD.Quantity);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Inventory', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the warehouse table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER warehouse_audit 
after insert or delete
ON warehouse
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'WarehouseId: ' || TO_CHAR(:NEW.WarehouseId) || ', WarehouseName: ' || :NEW.WarehouseName || ', Warehouse Address: ' || :NEW.WarehouseAddress;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Warehouse', null, 'Insert', null, insertedData);
    END IF;
    
    IF DELETING THEN
        deletedData := 'WarehouseId: ' || TO_CHAR(:OLD.WarehouseId) || ', WarehouseName: ' || :OLD.WarehouseName || ', Warehouse Address: ' || :OLD.WarehouseAddress;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Warehouse', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the customer table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Customer_audit 
after insert or update or delete
ON customer
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(200);
    deletedData VARCHAR2(200);
BEGIN
    IF INSERTING THEN
        insertedData := 'CustomerId: ' || TO_CHAR(:NEW.CustomerId) || ', CustomerFN: ' || :NEW.Customerfn || ', CustomerLN: ' || :NEW.Customerln || ', Customer Email: ' || :NEW.CustomerEmail || ', Customer Address: ' || :NEW.CustomerAddress;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Customer', null, 'Insert', null, insertedData);
    END IF;
    
    IF UPDATING THEN
        IF :OLD.CustomerEmail != :NEW.CustomerEmail THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Customer', 'CustomerEmail', 'Update', :OLD.CustomerEmail, :NEW.CustomerEmail);
        END IF;
        IF :OLD.CustomerAddress != :NEW.CustomerAddress THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Customer', 'CustomerAddress', 'Update', :OLD.CustomerAddress, :NEW.CustomerAddress);
        END IF;
    END IF;
    
    IF DELETING THEN
        deletedData := 'CustomerId: ' || TO_CHAR(:OLD.CustomerId) || ', CustomerFN: ' || :OLD.Customerfn || ', CustomerLN: ' || :OLD.Customerln || ', Customer Email: ' || :OLD.CustomerEmail || ', Customer Address: ' || :OLD.CustomerAddress;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Customer', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the store table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Store_audit 
after insert or delete
ON Store
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'StoreId: ' || TO_CHAR(:NEW.StoreId) || ', Store Name: ' || :NEW.StoreName;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Store', null, 'Insert', null, insertedData);
    END IF;
    
    IF DELETING THEN
        deletedData := 'StoreId: ' || TO_CHAR(:OLD.StoreId) || ', Store Name: ' || :OLD.StoreName;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Store', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the review table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Review_audit 
after insert or update or delete
ON review
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'ReviewId: ' || TO_CHAR(:NEW.ReviewId) || ', ProductId: ' || TO_CHAR(:NEW.ProductId) || ', CustomerId: ' || TO_CHAR(:NEW.CustomerId) || ', Review Flag: ' || TO_CHAR(:NEW.ReviewFlag) || ', Product Review: ' || TO_CHAR(:NEW.ProductReview) || ', Review Description: ' || :NEW.ReviewDescription;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Review', null, 'Insert', null, insertedData);
    END IF;
    
    IF UPDATING THEN
        IF :OLD.ReviewFlag != :NEW.ReviewFlag THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Review', 'ReviewFlag', 'Update', :OLD.ReviewFlag, :NEW.ReviewFlag);
        END IF;
        IF :OLD.ProductReview != :NEW.ProductReview THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Review', 'ProductReview', 'Update', :OLD.ProductReview, :NEW.ProductReview);
        END IF;
        IF :OLD.ReviewDescription != :NEW.ReviewDescription THEN
            INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Review', 'ReviewDescription', 'Update', :OLD.ReviewDescription, :NEW.ReviewDescription);
        END IF;
    END IF;
    
    IF DELETING THEN
        deletedData := 'ReviewId: ' || TO_CHAR(:OLD.ReviewId) || ', ProductId: ' || TO_CHAR(:OLD.ProductId) || ', CustomerId: ' || TO_CHAR(:OLD.CustomerId) || ', Review Flag: ' || TO_CHAR(:OLD.ReviewFlag) || ', Product Review: ' || TO_CHAR(:OLD.ProductReview) || ', Review Description: ' || :OLD.ReviewDescription;
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Review', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the orders table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Orders_audit 
after insert or delete
ON orders
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'OrderId: ' || TO_CHAR(:NEW.OrderId) || ', CustomerId: ' || TO_CHAR(:NEW.CustomerId) || ', StoreId: ' || TO_CHAR(:NEW.StoreId) || ', Order Date: ' || TO_CHAR(:NEW.OrderDate);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Orders', null, 'Insert', null, insertedData);
    END IF;
    
    IF DELETING THEN
        deletedData := 'OrderId: ' || TO_CHAR(:OLD.OrderId) || ', CustomerId: ' || TO_CHAR(:OLD.CustomerId) || ', StoreId: ' || TO_CHAR(:OLD.StoreId) || ', Order Date: ' || TO_CHAR(:OLD.OrderDate);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Orders', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the product_store table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Product_Store_audit 
after insert or delete
ON product_store
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'StoreId: ' || TO_CHAR(:NEW.StoreId) || ', ProductId: ' || TO_CHAR(:NEW.ProductId);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Product_Store', null, 'Insert', null, insertedData);
    END IF;
    
    IF DELETING THEN
        deletedData := 'StoreId: ' || TO_CHAR(:OLD.StoreId) || ', ProductId: ' || TO_CHAR(:OLD.ProductId);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Product_Store', null, 'Delete', deletedData, null);
    END IF;
END;
/

/**
    A trigger on the orders_product table after a transaction
    that inserts into the audit table
*/
CREATE OR REPLACE TRIGGER Orders_product_audit 
after insert or delete
ON orders_product
FOR EACH ROW
DECLARE
    insertedData VARCHAR2(150);
    deletedData VARCHAR2(150);
BEGIN
    IF INSERTING THEN
        insertedData := 'ProductId: ' || TO_CHAR(:NEW.ProductId) || ', OrderId: ' || TO_CHAR(:NEW.OrderId) || ', Quantity: ' || TO_CHAR(:NEW.Quantity);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Orders_Product', null, 'Insert', null, insertedData);
    END IF;
    
    IF DELETING THEN
        deletedData := 'ProductId: ' || TO_CHAR(:OLD.ProductId) || ', OrderId: ' || TO_CHAR(:OLD.OrderId) || ', Quantity: ' || TO_CHAR(:OLD.Quantity);
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (CURRENT_DATE, USER, 'Orders_Product', null, 'Delete', deletedData, null);
    END IF;
END;
/  

---------------------------------------------------------------------------------------
-- TYPE CREATION!

-- Create the Warehouse table!
CREATE OR REPLACE TYPE WARTYP AS OBJECT (
  --WarehouseId         NUMBER,             
  WarehouseName         VARCHAR2(20),
  WarehouseAddress      VARCHAR2(100)
);
/

-- Create the Product table!
CREATE OR REPLACE TYPE PROTYP AS OBJECT (
  --ProductId           NUMBER,             
  ProductName           VARCHAR2(20),
  ProductCategory       VARCHAR2(20),
  Price                 NUMBER(10, 2)
);
/

-- Create the Inventory table!
CREATE OR REPLACE TYPE INVTYP AS OBJECT (
  ProductId             NUMBER,
  WarehouseId           NUMBER,
  Quantity              NUMBER
);
/

-- Create the Customer table!
CREATE OR REPLACE TYPE CUSTYP AS OBJECT (
  --CustomerId          NUMBER,            
  CustomerFN            VARCHAR2(20),
  CustomerLN            VARCHAR2(20),
  CustomerEmail         VARCHAR2(50),
  CustomerAddress       VARCHAR2(50)
);
/

-- Create the Review table!
CREATE OR REPLACE TYPE REVTYP AS OBJECT (
  --ReviewId            NUMBER,            
  ProductId             NUMBER,
  CustomerId            NUMBER,
  -- ReviewFlag         NUMBER(6),       -- Spawns null 
  ProductReview         NUMBER(1),       
  ReviewDescription     VARCHAR2(150)
);
/

-- Create the Store table!
CREATE OR REPLACE TYPE STOTYP AS OBJECT(
    --StoreId           NUMBER,                    
    StoreName           VARCHAR2(20)
);
/

-- Create the Order table!
CREATE OR REPLACE TYPE ORDTYP AS OBJECT (
  --OrderId             NUMBER,         
  CustomerId            NUMBER,
  StoreId               NUMBER,
  OrderDate             DATE
);
/

-- Create the Orders_Product table!
CREATE OR REPLACE TYPE OPTYP AS OBJECT (
  ProductId             NUMBER,
  OrderId               NUMBER,
  Quantity              NUMBER
);
/

-- Create the Product_Store table!
CREATE OR REPLACE TYPE PSTYP AS OBJECT (
  StoreId             NUMBER,
  ProductId           NUMBER
);
/

-- Create the Audit table
CREATE OR REPLACE TYPE AUDTYP AS OBJECT (          
  AuditId               NUMBER,
  AuditTime             DATE,
  UserId                VARCHAR2(10),
  TableName             VARCHAR2(50),
  ColumnName            VARCHAR2(50),
  Operation             VARCHAR2(15),
  OldValue              VARCHAR2(150),
  NewValue              VARCHAR2(150)
);
/




----------------------------------------------------------------------------------------
-- SAMPLE INSERTIONS! 

-- Insert data into the Warehouse table
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('warehouse A', '100 rue William, saint laurent, Quebec, Canada');
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('Warehouse B', '304 Rue Fran�ois-Perrault, Villera Saint-Michel, Montr�al, QC');
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('Warehouse C', '86700 Weston Rd, Toronto, Canada');
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('Warehouse D', '170 Sideroad, Quebec City, Canada');
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('Warehouse E', '1231 Trudea road, Ottawa, Canada');
INSERT INTO Warehouse (WarehouseName, WarehouseAddress) VALUES ('Warehouse F', '16 Whitlock Rd, Alberta, Canada');

-- Insert data into the store table
INSERT INTO Store (StoreName) VALUES ('Marche Adonis');
INSERT INTO Store (StoreName) VALUES ('Marche Atwater');
INSERT INTO Store (StoreName) VALUES ('Dawson Store');
INSERT INTO Store (StoreName) VALUES ('Store Magic');
INSERT INTO Store (StoreName) VALUES ('Movie Store');
INSERT INTO Store (StoreName) VALUES ('Super Rue Champlain');
INSERT INTO Store (StoreName) VALUES ('Toy R Us');
INSERT INTO Store (StoreName) VALUES ('Dealer One');
INSERT INTO Store (StoreName) VALUES ('Dealer Montreal');
INSERT INTO Store (StoreName) VALUES ('Movie Start');
INSERT INTO Store (StoreName) VALUES ('Star Store');

-- Products
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Laptop Asus 104S', 'Electronics', 970);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Apple', 'Grocery', 5);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Sims CD', 'Video Game', 16.67);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Orange', 'Grocery', 2);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Barbie Movie', 'DVD', 30);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('L''Oreal Normal Hair', 'Health', 10);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('BMW IX Lego', 'Toys', 40);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('BMW I6', 'Cars', 50000);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Truck 500c', 'Vehicle', 856600);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Paper Towel', 'Beauty', 16.67);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Plum', 'Grocery', 1.67);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Lamborghini Lego', 'Health', 40);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Chicken', 'Grocery', 45);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Pasta', 'Grocery', 4.5);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('PS5', 'Electronics', 200);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('Tomato', 'Grocery', 5);
INSERT INTO Product (ProductName, ProductCategory, Price) VALUES ('TrainX745', 'Vehicle', 50000);

-- Insert data into the Customers table
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('mahsa', 'sadeghi', 'msadeghi@dawsoncollege.qc.ca', 'dawson, montreal, quebec, canada');--
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('alex', 'brown', 'alex@gmail.com', '090 boul saint laurent, montreal, quebec, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('martin', 'alexandre', 'marting@yahoo.com', 'brossard, quebec, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('daneil', 'hanne', 'daneil@yahoo.com', '100 atwater street, toronto, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('mahsa', 'sadeghi', 'msadeghi@dawsoncollege.qc.ca', 'dawson, montreal, quebec, canada'); --
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('John', 'boura', 'bdoura@gmail.com', '100 Young street, toronto, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('Ari', 'brown', 'b.a@gmail.com', NULL);
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('martin', 'Li', 'm.li@gmail.com', '87 boul saint laurent, montreal, quebec, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('olivia', 'smith', 'smith@hotmail.com', '76 boul decalthon, laval, quebec, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('Amanda', 'Harry', 'am.harry@yahioo.com', '100 boul, montreal, quebec, canada'); -- causing issue
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('Jack', 'Jonhson', 'johnson.a@gmail.com', 'Calgary, Alberta, Canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('martin', 'alexandre', 'marting@yahoo.com', 'brossard, quebec, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('mahsa', 'sadeghi', 'ms@gmail.com', '104 gill street, Toronto, Canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('John', 'belle', 'abcd@yahoo.com', '105 Young street, toronto, canada');
INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES ('Noah', 'Garcia', 'g.noah@yahoo.com', '22222 happy street, Laval, quebec, canada');

-- Insert data into the Product_Store table
INSERT INTO Product_Store (StoreId, ProductId) VALUES (1, 1);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (2, 2);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (3, 3);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (4, 4);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (5, 5);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (6, 6);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (7, 7);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (8, 8);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (9, 9);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (10, 10);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (2, 11);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (6, 6);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (7, 12);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (2, 11);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (7, 12);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (8, 8);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (10, 3);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (7, 5);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (1, 13);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (2, 14);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (11, 15);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (7, 7);
INSERT INTO Product_Store (StoreId, ProductId) VALUES (4, 14);

-- Insert data into the Orders table
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (1, 1, TO_DATE('4/21/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (2, 2, TO_DATE('10/23/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (3, 3, TO_DATE('10/1/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (4, 4, TO_DATE('10/23/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (2, 5, TO_DATE('10/23/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (3, 6, TO_DATE('10/10/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (1, 7, TO_DATE('10/11/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (6, 8, TO_DATE('10/10/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (7, 9, NULL);
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (10, 10, NULL);
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (11, 2, TO_DATE('5/6/2020', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (3, 6, TO_DATE('9/12/2019', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (1, 7, TO_DATE('10/11/2010', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (1, 2, TO_DATE('5/6/2022', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (13, 7, TO_DATE('10/7/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (14, 8, TO_DATE('8/10/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (2, 5, TO_DATE('10/23/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (2, 7, TO_DATE('10/2/2023', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (8, 1, TO_DATE('4/3/2019', 'MM/DD/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (9, 2, TO_DATE('29/12/2021', 'DD/MM/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (15, 11, TO_DATE('20/1/2020', 'DD/MM/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (13, 7, TO_DATE('10/11/2022', 'DD/MM/YYYY'));
INSERT INTO Orders (CustomerId, StoreId, OrderDate) VALUES (9, 4, TO_DATE('29/12/2021', 'DD/MM/YYYY'));

-- Insert data into the  Orders_Product table
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (1, 1, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (2, 2, 2);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (3, 3, 3);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (4, 4, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (5, 5, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (6, 6, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (7, 7, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (8, 8, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (9, 9, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (10, 10, 3);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (11, 11, 6);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (6, 12, 3);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (12, 13, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (11, 14, 7);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (12, 15, 2);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (8, 16, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (3, 17, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (5, 18, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (13, 19, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (14, 20, 3);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (15, 21, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (7, 22, 1);
INSERT INTO  Orders_Product (ProductId, OrderId, Quantity) VALUES (14, 23, 3);

-- Inventory

INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (1, 1, 1000);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (2, 2, 24980);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (3, 3, 103);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (4, 4, 35405);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (5, 5, 40);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (6, 6, 450);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (7, 1, 10);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (8, 1, 6);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (9, 5, 1000);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (10, 6, 3532);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (11, 3, 43242);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (10, 2, 39484);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (11, 4, 6579);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (12, 5, 98765);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (13, 6, 43523);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (14, 1, 2132);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (15, 4, 123);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (16, 1, 352222);
INSERT INTO Inventory (ProductId, WarehouseId, Quantity) VALUES (17, 5, 4543);

-- Insert data into the Review table
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (1, 1, 3, 4, 'it was affordable.');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (2, 2, 0, 3, 'quality was not good');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (2, 3, 1, 2, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (4, 4, 0, 5, 'highly recommend');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (5, 2, 0, 1, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (6, 3, 0, 1, 'did not worth the price');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (7, 1, 0, 1, 'missing some parts');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (8, 6, 1, 5, 'trash');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (9, 7, 0, 2, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (10, 10, 0, 5, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (11, 11, 0, 4, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (6, 3, 0, 3, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (12, 1, 0, 1, 'missing some parts');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (11, 1, 0, 4, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (12, 13, 0, 1, 'great product');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (8, 14, 1, 5, 'bad quality');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (3, 2, 0, 1, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (5, 2, 0, 4, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (13, 8, 0, 4, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (14, 9, 0, 5, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (7, 1, 2, 1, 'worse car i have driven!');
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (14, 9, 0, 4, null);
INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (1, 1, 10, 4, 'I HATE THIS PRODUCT ');
