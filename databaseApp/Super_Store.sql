-- Samin Ahmed 2043024 
-- Final Project


CREATE OR REPLACE PACKAGE Super_Store AS 

    -- Insertions into table with TYPES!                     
     PROCEDURE insert_warehouse(vWarehouse WARTYP);              
     PROCEDURE insert_inventory(vInventory INVTYP);              
     PROCEDURE insert_product(vProduct PROTYP);                  
     PROCEDURE insert_customer(vCustomer CUSTYP);                
     PROCEDURE insert_review(vReview REVTYP);                    
     PROCEDURE insert_store(vStore STOTYP);                      
     PROCEDURE insert_orders(vOrders ORDTYP);                    
     PROCEDURE insert_orders_product(vOrders_Product OPTYP);     
     PROCEDURE insert_product_store(vProduct_Store PSTYP);       
     PROCEDURE insert_auditTable(vAudit AUDTYP);    
     
     -- Getter procedures
    PROCEDURE get_products(products OUT SYS_REFCURSOR);
    PROCEDURE get_productsEnhanced(products OUT SYS_REFCURSOR, category IN VARCHAR2);
    PROCEDURE get_total_quantity_at_warehouses(quantity OUT SYS_REFCURSOR);
    PROCEDURE get_orders(orders OUT SYS_REFCURSOR, customerid IN NUMBER, orDate in DATE, storeid IN NUMBER);
    FUNCTION get_average_review(productIdRef NUMBER) RETURN NUMBER;
    PROCEDURE get_auditlog(logs OUT SYS_REFCURSOR);
    
    PROCEDURE get_flaggedCustomers(problemUsers OUT SYS_REFCURSOR);
    PROCEDURE get_flaggedReviews(reviews OUT SYS_REFCURSOR);
    
    -- Update Procedures
    PROCEDURE update_warehouse_quantity(vInventory INVTYP);
    PROCEDURE update_review(reviewIdRef NUMBER, reviewFlagRef NUMBER, reviewNumber NUMBER, reviewText review.reviewDescription%TYPE);
    PROCEDURE update_flagged_review(reviewIdRef NUMBER, reviewFlagRef NUMBER, reviewNumber NUMBER, reviewText review.reviewDescription%TYPE);
    PROCEDURE update_product_category(productIdRef NUMBER, categoryRef product.productcategory%TYPE);
    PROCEDURE update_product_name(productIdRef NUMBER, productNameRef product.productName%TYPE);

    -- Delete Procedures
    PROCEDURE delete_order(orderIdRef NUMBER);
    PROCEDURE delete_review(reviewIdRef NUMBER);
    PROCEDURE delete_flagged_review(reviewIdRef NUMBER);
    PROCEDURE delete_warehouse(warehouseIdRef NUMBER);

    -- Exceptions we use
    PRODUCT_DOES_NOT_EXIST EXCEPTION;
    WAREHOUSE_DOES_NOT_EXIST EXCEPTION;
    CUSTOMER_DOES_NOT_EXIST EXCEPTION;
    STORE_DOES_NOT_EXIST EXCEPTION;
    ORDER_DOES_NOT_EXIST EXCEPTION;
    ALREADY_EXISTS EXCEPTION;
    
END Super_Store; 
/

CREATE OR REPLACE PACKAGE BODY Super_Store AS





/**
    A procedure that creates a warehouse.
    @ param vWarehouse WARTYP 
*/
    PROCEDURE insert_warehouse(vWarehouse IN WARTYP)
    IS 
    BEGIN 
        INSERT INTO WAREHOUSE (WarehouseName, WarehouseAddress)VALUES(
            vWarehouse.WarehouseName,
            vWarehouse.WarehouseAddress
        );
        COMMIT; 

    EXCEPTION
        WHEN OTHERS THEN   
           raise_application_error(-20950, 'An unknown error occured.');
    END insert_warehouse; 

/**
    A procedure that creates an inventory row.
    @ param vInventory INVTYP
*/
   PROCEDURE insert_inventory(vInventory IN INVTYP) AS
        product_exists NUMBER;
        warehouse_exists NUMBER;
        row_exists NUMBER;
    BEGIN
        -- Check if the Product with the provided ProductId exists
        SELECT COUNT(*) INTO product_exists FROM Product WHERE ProductId = vInventory.ProductId;
        
        -- Check if the Warehouse with the provided WarehouseId exists
        SELECT COUNT(*) INTO warehouse_exists FROM Warehouse WHERE WarehouseId = vInventory.WarehouseId;
        
        SELECT COUNT(*) INTO row_exists FROM Inventory WHERE ProductId = vInventory.ProductId AND WarehouseId = vInventory.WarehouseId; 


        -- If either the Product or Warehouse does not exist, raise an error
        IF product_exists = 0 THEN
            RAISE PRODUCT_DOES_NOT_EXIST;
        ELSIF warehouse_exists = 0 THEN
            RAISE WAREHOUSE_DOES_NOT_EXIST;
        ELSIF row_exists > 0 THEN
            UPDATE Inventory SET quantity = vInventory.quantity
            WHERE ProductId = vInventory.ProductId AND WarehouseId = vInventory.WarehouseId;
        ELSE 
            -- Insert data into the Inventory table
            INSERT INTO Inventory (ProductId, WarehouseId, Quantity)
            VALUES (vInventory.ProductId, vInventory.WarehouseId, vInventory.Quantity);
            COMMIT;
        END IF;
    EXCEPTION
        WHEN PRODUCT_DOES_NOT_EXIST THEN
            raise_application_error(-20999, 'Product does not exist');
        WHEN WAREHOUSE_DOES_NOT_EXIST THEN
            raise_application_error(-20998, 'Warehouse does not exist');
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END insert_inventory;
    
/**
    A procedure that creates a product
    @ param vProduct PROTYP 
*/
    PROCEDURE insert_product(vProduct IN PROTYP) AS
    
        BEGIN 
            INSERT INTO Product (ProductName, ProductCategory, Price) VALUES(
                vProduct.ProductName,
                vProduct.ProductCategory,
                vProduct.Price
            );
            COMMIT; 

        EXCEPTION
            WHEN OTHERS THEN 
              raise_application_error(-20950, 'An unknown error occured.');
        END insert_product; 
        
/**
    A procedure that creates a customer
    @ param vCustomer CUSTYP
*/
    PROCEDURE insert_customer(vCustomer IN CUSTYP) AS 

    BEGIN  
        INSERT INTO Customer (CustomerFN, CustomerLN, CustomerEmail, CustomerAddress) VALUES(
            vCustomer.CustomerFN,
            vCustomer.CustomerLN,
            vCustomer.CustomerEmail,
            vCustomer.CustomerAddress
        );
        COMMIT; 
    EXCEPTION 
        WHEN OTHERS THEN       
           raise_application_error(-20950, 'An unknown error occured.');
    END insert_customer; 
    
/**
    A procedure that creates a review
    @ param vReview REVTYP
*/
    PROCEDURE insert_review(vReview IN REVTYP) AS            
        product_exists      NUMBER; 
        customer_exists     NUMBER;
        product_valid       NUMBER;
    BEGIN

        -- Check product count to make sure it exists (1)
        SELECT COUNT(*) INTO product_exists FROM Product WHERE ProductId = vReview.ProductId; 
        -- Check Customer count to make sure it exists (1) 
        SELECT COUNT(*) INTO customer_exists FROM Customer WHERE CustomerId = vReview.CustomerId; 
    
        IF vReview.ProductReview = -1 THEN
            product_valid := NULL;
        ELSE
            product_valid := vReview.ProductReview;
        END IF;

        IF product_exists = 0 THEN  
            RAISE PRODUCT_DOES_NOT_EXIST;
        ELSIF customer_exists = 0 THEN  
            RAISE CUSTOMER_DOES_NOT_EXIST; 
        ELSE 
            INSERT INTO Review (ProductId, CustomerId, ReviewFlag, ProductReview, ReviewDescription) VALUES (
                vReview.ProductId,
                vReview.CustomerId,
                0,
                product_valid,
                vReview.ReviewDescription
            );
            COMMIT; 
        END IF;

    EXCEPTION   
        WHEN PRODUCT_DOES_NOT_EXIST THEN    
            raise_application_error(-20999, 'Product does not exist');
        WHEN CUSTOMER_DOES_NOT_EXIST THEN  
            raise_application_error(-20997, 'Customer does not exist'); 
        WHEN OTHERS THEN 
            raise_application_error(-20950, 'An unknown error occured.');
    END insert_review; 


/**
    A procedure that creates a store
    @ param vStore STOTYP
*/
    PROCEDURE insert_store(vStore STOTYP) AS
    BEGIN  
        INSERT INTO Store (StoreName) VALUES(
            vStore.StoreName
        );
        COMMIT; 
    EXCEPTION 
        WHEN OTHERS THEN       
          raise_application_error(-20950, 'An unknown error occured.');
    END insert_store; 


/**
    A procedure that creates an order
    @ param vOrders ORDTYP
*/
    PROCEDURE insert_orders(vOrders IN ORDTYP) AS
        customer_exists NUMBER;
        store_exists NUMBER;
    BEGIN 
        -- Check if customer exists (1) 
        SELECT COUNT(*) INTO customer_exists FROM CUSTOMER WHERE CustomerId = vOrders.CustomerId; 
        -- Check if store exists (1) 
        SELECT COUNT(*) INTO store_exists FROM Store WHERE StoreId = vOrders.StoreId; 

        IF customer_exists = 0 THEN
            RAISE CUSTOMER_DOES_NOT_EXIST;
        ELSIF store_exists = 0 THEN 
            RAISE STORE_DOES_NOT_EXIST; 
        ELSE 
            INSERT INTO Orders(CustomerId, StoreId, OrderDate) VALUES (vOrders.CustomerId, vOrders.StoreId, vOrders.OrderDate);
            COMMIT; 
        END IF;
        
    EXCEPTION 
        WHEN CUSTOMER_DOES_NOT_EXIST THEN
            raise_application_error(-20997, 'Customer does not exist');
        WHEN STORE_DOES_NOT_EXIST THEN
            raise_application_error(-20996, 'Store does not exist');
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END insert_orders; 


/**
    A procedure that creates an orders_product row
    @ param vOrders_Product OPTYP
*/
    PROCEDURE insert_orders_product(vOrders_Product IN OPTYP) AS
        product_exists NUMBER; 
        order_exists NUMBER;
    BEGIN 
        SELECT COUNT(*) INTO product_exists FROM Product  WHERE ProductId = vOrders_Product.ProductId; 
        SELECT COUNT(*) INTO order_exists FROM Orders  WHERE OrderId = vOrders_Product.OrderId;

        IF product_exists = 0 THEN  
           RAISE PRODUCT_DOES_NOT_EXIST;
        ELSIF order_exists = 0 THEN
            RAISE ORDER_DOES_NOT_EXIST;
        ELSE 
            INSERT INTO Orders_Product(ProductId, OrderId, Quantity) VALUES (
                vOrders_Product.ProductId,
                vOrders_Product.OrderId, 
                vOrders_Product.Quantity
            );
            COMMIT; 
        END IF;
        
    EXCEPTION
        WHEN PRODUCT_DOES_NOT_EXIST THEN
            raise_application_error(-20999, 'Product does not exist.');
        WHEN ORDER_DOES_NOT_EXIST THEN
            raise_application_error(-20995, 'Order does not exist');
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END insert_orders_product;

/**
    A procedure that creates a product_store row
    @ param vProduct_Store PSTYP
*/
    PROCEDURE insert_product_store(vProduct_Store PSTYP) AS 
        product_exists NUMBER;
        store_exists NUMBER;
        row_exists NUMBER;
    BEGIN 
        SELECT COUNT(*) INTO product_exists FROM Product WHERE ProductId = vProduct_Store.ProductId; 
        SELECT COUNT(*) INTO store_exists FROM Store WHERE StoreId = vProduct_Store.StoreId; 
        SELECT COUNT(*) INTO row_exists FROM Product_Store WHERE StoreId = vProduct_Store.StoreId AND ProductId = vProduct_Store.ProductId;

        IF product_exists = 0 THEN
            RAISE PRODUCT_DOES_NOT_EXIST;
        ELSIF store_exists = 0 THEN
            RAISE STORE_DOES_NOT_EXIST;
        ELSIF row_exists > 0 THEN
            RAISE ALREADY_EXISTS;
        ELSE
            INSERT INTO Product_Store VALUES(vProduct_Store.StoreId, vProduct_Store.ProductId); 
            COMMIT; 
        END IF; 

    EXCEPTION 
        WHEN PRODUCT_DOES_NOT_EXIST THEN
            raise_application_error(-20999, 'Product does not exist');
        WHEN STORE_DOES_NOT_EXIST THEN
            raise_application_error(-20996, 'Store does not exist'); 
        WHEN ALREADY_EXISTS THEN
            raise_application_error(-20994, 'Row already exists');
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
            
    END insert_product_store; 
  
/**
    A procedure that creates a warehouse.
    @ param vWarehouse WARTYP 
*/
    PROCEDURE insert_auditTable(vAudit AUDTYP) AS
    BEGIN 
        INSERT INTO AuditTable (AuditTime, UserId, TableName, ColumnName, Operation, OldValue, NewValue) VALUES (
            CURRENT_DATE,
            vAudit.UserId,
            vAudit.TableName,
            vAudit.ColumnName,
            vAudit.Operation,
            vAudit.OldValue,
            vAudit.NewValue
        );
        COMMIT; 
    END insert_auditTable;
    

/**
    A procedure that outputs a cursor of products
    @ param products, the cursor that will be filled and given back to the caller.
*/
    PROCEDURE get_products(products OUT SYS_REFCURSOR)
    AS
    BEGIN
        OPEN products FOR
            SELECT ProductId AS "ProductId",
                   ProductName AS "ProductName",
                   ProductCategory AS "ProductCategory",
                   Price AS "Price"
            FROM Product;
    END get_products;
    

/**
    A procedure that outputs a cursor of products with an optional category filter. 
    @ param products, the cursor that will be filled and given back to the caller.
    @ param category, the category filter, which might be null if there is no filtering. 
*/
    PROCEDURE get_productsEnhanced(products OUT SYS_REFCURSOR, category IN VARCHAR2)
    AS
        my_query VARCHAR2(500); 
    BEGIN
        my_query := 'SELECT ProductId AS "ProductId",
                   ProductName AS "ProductName",
                   ProductCategory AS "ProductCategory",
                   Price AS "Price" FROM Product ';
                   
        IF category IS NOT NULL THEN
            my_query := my_query || 'WHERE ProductCategory = ' || '''' || category || ''''; 
        END IF;
        
        OPEN products FOR my_query; 
      
      EXCEPTION  
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_productsEnhanced; 
    
    
/**
    A procedure that outputs a cursor that has the total quantity of products available at warehouses
    @ param quantity, the cursor that will be filled and given back to the caller.
*/
    PROCEDURE get_total_quantity_at_warehouses(quantity OUT SYS_REFCURSOR)
    AS
    BEGIN
        OPEN quantity FOR
                SELECT 
                        P.ProductId AS "ProductId", 
                        P.ProductName AS "ProductName" , 
                        NVL(SUM(I.Quantity), 0) AS "TotalAvailable"
                FROM 
                        Product P  LEFT JOIN Inventory I ON (P.ProductID = I.ProductID)
                    
                GROUP BY P.ProductId, P.ProductName;
                
    EXCEPTION  
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_total_quantity_at_warehouses;
    
    

/**
    A procedure that outputs a cursor with all the audit logs
    @ param logs, the cursor that will be filled and given back to the caller.
*/
    PROCEDURE get_auditlog(logs OUT SYS_REFCURSOR)
    AS
    BEGIN
        OPEN logs FOR
            SELECT 
                AuditId AS "AuditId",
                AuditTime AS "AuditTime", 
                UserId AS "UserId",
                TableName AS "TableName",
                ColumnName AS "ColumnName",
                Operation AS "Operation",
                OldValue AS "OldValue",
                NewValue AS "NewValue"
            FROM AuditTable
            ORDER BY 
                AuditTime DESC; 
    EXCEPTION  
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_auditlog;
    

/**
    A procedure that outputs a cursor of flagged customers
    @ param problemUsers, the cursor that will be filled and given back to the caller.
*/
    PROCEDURE get_flaggedCustomers(problemUsers OUT SYS_REFCURSOR)
    AS
    BEGIN
        OPEN problemUsers FOR
            SELECT 
                Customer.CustomerId AS "CustomerId",
                Customer.CustomerFN AS "FirstName",
                Customer.CustomerLN AS "LastName",
                Customer.CustomerEmail AS "Email",
                Customer.CustomerAddress AS "Address",
                COUNT(Review.reviewId) AS "TotalProblematicReviews"
            FROM 
                Customer
            INNER JOIN Review ON Customer.CustomerId = Review.CustomerId
            WHERE 
                Review.reviewFlag > 1
            GROUP BY
                Customer.CustomerId, 
                Customer.CustomerFN, 
                Customer.CustomerLN, 
                Customer.CustomerEmail, 
                Customer.CustomerAddress;
    EXCEPTION  
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_flaggedCustomers;
    

/**
    A procedure that outputs a cursor of flagged reviews
    @ param reviews, the cursor that will be filled and given back to the caller.
*/
    PROCEDURE get_flaggedReviews(reviews OUT SYS_REFCURSOR)
    AS
    BEGIN
        OPEN reviews FOR
            SELECT 
                ReviewId AS "ReviewId",
                ProductId AS "ProductId",
                CustomerId AS "CustomerId",
                ReviewFlag AS "ReviewFlag",
                ProductReview AS "ProductReview",
                ReviewDescription AS "ReviewDescription"
            FROM
                Review
            WHERE
                ReviewFlag > 1; 
    EXCEPTION  
    
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_flaggedReviews;
    

/**
    A procedure that outputs a cursor of orders with optional filters 
    @ param orders, the cursor that will be filled and given back to the caller.
    @ param customerid, the customerid filter (may be -1, meaning no filtering)
    @ param orDate, the optional date filter (may be null, meaning no filtering)
    @ param storeId, the optional store filter (may be -1, meaning no filtering)
*/
    PROCEDURE get_orders(orders OUT SYS_REFCURSOR, customerid IN NUMBER, orDate in DATE, storeid IN NUMBER)
    AS
        my_query VARCHAR2(500);
        num NUMBER; 
    BEGIN
        my_query := 'SELECT Orderid AS "OrderId",
                            CustomerId AS "CustomerId",
                            StoreId AS "StoreId",
                            OrderDate AS "OrderDate" 
                    FROM 
                            ORDERS ';
                   
        num := 0; 
        
        IF customerid != -1 THEN 
            my_query := my_query || 'WHERE CustomerId = ' || '''' || customerid || ''''; 
            num := num+1; 
        END IF;
        
        IF orDate IS NOT NULL THEN
            IF num = 1 THEN
                my_query := my_query || ' AND OrderDate = ' || '''' || orDate || '''';
            ELSIF num = 0 THEN 
                my_query := my_query || ' WHERE OrderDate = ' || '''' || orDate || '''';
            END IF;
            num := num +1; 
        END IF; 
        
        IF storeid != -1 THEN
            IF num > 0 THEN
                my_query := my_query || ' AND StoreId = ' || '''' || storeid || ''''; 
            ELSIF num = 0 THEN
                my_query := my_query || ' WHERE StoreId = ' || '''' || storeid || '''';
            END IF;
        END IF; 
        
        OPEN orders FOR my_query; 
        
    EXCEPTION  
        WHEN OTHERS THEN
            raise_application_error(-20950, 'An unknown error occured.');
    END get_orders;
    

/**
    A function that returns the average review of a given productId
    @ param productIdRef : The productId for which you want the average review of 
*/
    FUNCTION get_average_review(productIdRef NUMBER)
    RETURN NUMBER AS
        averageReview NUMBER;
        prod_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO prod_exists FROM product
        WHERE productId = productIdRef;
        IF prod_exists = 0 THEN
            raise_application_error(-20001, 'Product does not exist');
        END IF;
        
        SELECT
            AVG(productReview)
        INTO
            averageReview
        FROM
            review
        WHERE
            productId = productIdRef;
        RETURN averageReview;    
    END get_average_review;
    
    PROCEDURE update_warehouse_quantity(vInventory INVTYP) AS
        prod_exists NUMBER;
        ware_exists NUMBER;
        prod_ware_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO prod_ware_exists FROM inventory
        WHERE
            warehouseId = vInventory.warehouseId AND
            productId = vInventory.productId;
        SELECT COUNT(*) INTO prod_exists FROM product
        WHERE productId = vInventory.productId;
        SELECT COUNT(*) INTO ware_exists FROM warehouse
        WHERE warehouseId = vInventory.warehouseId;
        IF prod_exists = 0 THEN
            raise_application_error(-20001, 'Product does not exist');
        END IF;
        IF ware_exists = 0 THEN
            raise_application_error(-20002, 'Warehouse does not exist');
        END IF;
        IF prod_ware_exists = 0 THEN
            raise_application_error(-20003, 'This product is not in the warehouse inventory');
        END IF;
        
        UPDATE
            inventory
        SET
            quantity = vInventory.Quantity
        WHERE
            productid = vInventory.ProductId AND
            warehouseId = vInventory.WarehouseId;
    END update_warehouse_quantity;
    

    /**
    A procedure that updates a review
    @ param reviewIdRef, the review id of the review
    @ param reviewNumber, the new review (?/5) value
    @ param reviewText, the new review description
*/
    PROCEDURE update_review(reviewIdRef NUMBER, reviewFlagRef NUMBER, reviewNumber NUMBER, reviewText review.reviewDescription%TYPE) AS
        review_exists NUMBER;
        flag_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_exists FROM Review WHERE reviewId = reviewIdRef;
        SELECT COUNT(*) INTO flag_exists FROM Review
        WHERE reviewId = reviewIdRef AND reviewFlag > 1;
        IF review_exists = 0 THEN
            raise_application_error(-20004, 'ReviewId does not exist');
        END IF;
        IF flag_exists > 0 THEN
            raise_application_error(-20005, 'Review is flagged.');
        END IF;
        
        UPDATE
            review
        SET
            productReview = reviewNumber,
            reviewDescription = reviewText,
            reviewFlag = reviewFlagRef
        WHERE    
            reviewId = reviewIdRef;
    END update_review;
    

    /**
    A procedure that updates flagged reviews
    @ param reviewIdRef, the review id of the review
    @ param reviewNumber, the new review (?/5) value
    @ param reviewText, the new review description
*/
    PROCEDURE update_flagged_review(reviewIdRef NUMBER, reviewFlagRef NUMBER, reviewNumber NUMBER, reviewText review.reviewDescription%TYPE) AS
        review_exists NUMBER;
        flag_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_exists FROM Review WHERE reviewId = reviewIdRef;
        IF review_exists = 0 THEN
            raise_application_error(-20004, 'ReviewId does not exist');
        END IF;
        SELECT COUNT(*) INTO flag_exists FROM Review
        WHERE reviewId = reviewIdRef AND reviewFlag > 1;
        IF flag_exists = 0 THEN
            raise_application_error(-20006, 'Review is not flagged enough');
        END IF;
        
        UPDATE
            review
        SET
            productReview = reviewNumber,
            reviewDescription = reviewText,
            reviewFlag = reviewFlagRef
        WHERE    
            reviewId = reviewIdRef;
    END update_flagged_review;
    


 /**
    A procedure that updates a products' category
    @ param productIdRef, the productId of the product you want to update
    @ param categoryRef, the category you want to set the product to
*/
    PROCEDURE update_product_category(productIdRef NUMBER, categoryRef product.productcategory%TYPE) AS
        prod_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO prod_exists FROM product
        WHERE productId = productIdRef;
        IF prod_exists = 0 THEN
            raise_application_error(-20001, 'Product does not exist');
        END IF;
        
        UPDATE
            product
        SET
            productCategory = categoryRef
        WHERE
            productId = productIdRef;
    END update_product_category;
    

 /**
    A procedure that updates a product's name 
    @ param productIdRef, the productId of the product you want to update
    @ param productNameRef, the new product name 
*/
    PROCEDURE update_product_name(productIdRef NUMBER, productNameRef product.productName%TYPE) AS
        prod_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO prod_exists FROM product
        WHERE productId = productIdRef;
        IF prod_exists = 0 THEN
            raise_application_error(-20001, 'Product does not exist');
        END IF;
        
        UPDATE
            product
        SET
            productName = productNameRef
        WHERE
            productId = productIdRef;
    END update_product_name;
    
    PROCEDURE delete_order(orderIdRef NUMBER) AS
        order_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO order_exists FROM Orders WHERE orderId = orderIdRef;
        IF order_exists = 0 THEN
            raise_application_error(-20007, 'Order does not exist');
        END IF;
        
        DELETE FROM Orders_Product WHERE orderId = orderIdRef;
        DELETE FROM Orders WHERE orderId = orderIdRef;
    END delete_order;
    
    PROCEDURE delete_review(reviewIdRef NUMBER) AS
        review_exists NUMBER;
        flag_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_exists FROM Review WHERE reviewId = reviewIdRef;
        SELECT COUNT(*) INTO flag_exists FROM Review
        WHERE reviewId = reviewIdRef AND reviewFlag > 1;
        IF review_exists = 0 THEN
            raise_application_error(-20004, 'ReviewId does not exist');
        END IF;
        IF flag_exists > 0 THEN
            raise_application_error(-20005, 'Review is flagged.');
        END IF;
        
        DELETE FROM Review WHERE reviewId = reviewIdRef;
    END delete_review;
    
    PROCEDURE delete_flagged_review(reviewIdRef NUMBER) AS
        review_exists NUMBER;
        flag_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO review_exists FROM Review WHERE reviewId = reviewIdRef;
        IF review_exists = 0 THEN
            raise_application_error(-20004, 'ReviewId does not exist');
        END IF;
        SELECT COUNT(*) INTO flag_exists FROM Review
        WHERE reviewId = reviewIdRef AND reviewFlag > 1;
        IF flag_exists = 0 THEN
            raise_application_error(-20006, 'Review is not flagged enough');
        END IF;
        
        DELETE FROM Review WHERE reviewId = reviewIdRef; 
    END delete_flagged_review;
    
    PROCEDURE delete_warehouse(warehouseIdRef NUMBER) AS
        ware_exists NUMBER;
    BEGIN
        SELECT COUNT(*) INTO ware_exists FROM warehouse WHERE warehouseId = warehouseIdRef;
        IF ware_exists = 0 THEN
            raise_application_error(-20002, 'Warehouse does not exist');
        END IF;
        
        DELETE FROM Inventory WHERE warehouseId = warehouseIdRef;
        DELETE FROM Warehouse WHERE warehouseId = warehouseIdRef;
    END delete_warehouse;
END Super_Store;
/
